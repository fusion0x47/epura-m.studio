#ifndef AUDIO_EPURA_H_
	#define AUDIO_EPURA_H_

	#include "../Logger_Epura/Logger_Epura.h"
	#include "../Register_Epura/Register_Epura.h"
	#include "../SamdAudio/SamdAudio.h"

	#define NUM_AUDIO_CHANNELS 1

	class Audio_Epura{
		private:
			// objets
			Logger_Epura*		_Logger;
			SamdAudio*				_SamdAudio;
			Register_Epura*	_Register;
			
			// variables
			boolean _SoundFailed = false;
			char	_FileSoundName[6];


			// constantes
			const unsigned int sampleRate = 22050;
			const int Audio_Pin = A0;
			const int CHIPSELECT_SD					= SDCARD_SS_PIN;

			// fonctions
			void DelayWithPoll(uint32_t timeGap);

		public:
			//constructeur
			Audio_Epura(Logger_Epura* logger, Register_Epura* _Register, int soundId);

			// variables
			int		_SoundId;
			
			//fonctions
			void Play();
			void SetFileName(int soundId);
			
	};
#endif