#include "Audio_Fusion0x47.h"


Audio_Fusion0x47::Audio_Fusion0x47(Logger_Fusion0x47* logger, Register_Fusion0x47* _Register, int soundId){
	_Logger = logger;
	_SoundId = soundId;
	sprintf(_FileSoundName, "%u.047", soundId);
	_SamdAudio = new SamdAudio();
	if (_SamdAudio->begin(sampleRate, NUM_AUDIO_CHANNELS, CHIPSELECT_SD) == -1)
	{
		_Logger->Error(7, -10, "Sound Fail to initialize!", "ERR - Unable to Clean");
		exit(0);
	}

}

void Audio_Fusion0x47::DelayWithPoll(uint32_t timeGap) {
	uint32_t startTime = millis();
	while (millis() - startTime < timeGap) {
		ModbusRTUServer.poll();
	}
}

void Audio_Fusion0x47::Play(){
	if (_Logger->IsEnabledForLevel(1)) sprintf(_Logger->Message, "AudioPlayer play the sound on file '%s'", _FileSoundName);
	_Logger->Information(1);

	digitalWrite(LED_BUILTIN, HIGH);
	_SamdAudio->play(_FileSoundName, 0);
	DelayWithPoll(5000);
	digitalWrite(LED_BUILTIN, LOW);
}
