#ifndef AUDIO_FUSION0X47_H_
	#define AUDIO_FUSION0X47_H_

	#include "../Logger_Fusion0x47/Logger_Fusion0x47.h"
	#include "../Register_Fusion0x47/Register_Fusion0x47.h"
	#include "../SamdAudio/SamdAudio.h"

	#define NUM_AUDIO_CHANNELS 1

	class Audio_Fusion0x47{
		private:
			// objets
			Logger_Fusion0x47*		_Logger;
			SamdAudio*				_SamdAudio;
			Register_Fusion0x47*	_Register;
			
			// variables
			boolean _SoundFailed = false;
			char	_FileSoundName[6];


			// constantes
			const unsigned int sampleRate = 22050;
			const int Audio_Pin = A0;
			const int CHIPSELECT_SD					= SDCARD_SS_PIN;

			// fonctions
			void DelayWithPoll(uint32_t timeGap);

		public:
			//constructeur
			Audio_Fusion0x47(Logger_Fusion0x47* logger, Register_Fusion0x47* _Register, int soundId);

			// variables
			int		_SoundId;
			
			//fonctions
			void Play();
			void SetFileName(int soundId);
			
	};
#endif