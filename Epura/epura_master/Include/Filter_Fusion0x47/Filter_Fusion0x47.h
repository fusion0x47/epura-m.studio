#ifndef FILTER_FUSION0X47_H_
	#define FILTER_FUSION0X47_H_

	#include "./FilterData.h"
	#include "../Logger_Fusion0x47/Logger_Fusion0x47.h"
	#include "../Nextion_Fusion0x47/Nextion_Fusion0x47.h"
	#include "../Register_Fusion0x47/Register_Fusion0x47.h"
	#include "../Audio_Fusion0x47/Audio_Fusion0x47.h"

	class Filter_Fusion0x47{
		private:
			// variables		

			float			_CorrectionInputPressure;
			bool			_IsSoundAuthorized			= false;
			bool			_EnableReadOfInputs;
			float			_PressureHistory[20];
			int				_AnalogPrecisionInput		= 8191;
			unsigned long	_LastReadTime;
			unsigned long	_StateStartTime;
			int				_TimeGapAfterFanStop;
			int				_TimeGapAfterFanStart;
			int				_TimeGapAfterSound;
			int				_TimeBetweenRead;
			int				_TempsMsOverMaxPressure;
			float			_MaxPressureTrailingStop;
			unsigned long	_TimerOverPressure          = 0;
			float			_SensorMaxH20;
			int				_DepartSensor;
			bool			_RtcUpdated					= false;
			char*			_CodeVersion;
			bool			_CleanUpAtStartup			= false;
			bool			_StateModified;
			int				_ChildMode;

			// objets
			Logger_Fusion0x47*		_Logger;
			Nextion_Fusion0x47*		_Nextion;
			Register_Fusion0x47*	_Register;
			Audio_Fusion0x47*		_Audio;
			RTCZero*				_Rtc;

			// fonctions
			void InitObjects(Nextion_Fusion0x47* nextion, Logger_Fusion0x47* logger, Register_Fusion0x47* registers);
			void DelayWithPoll(uint32_t timeGap);
			void ReadSoundIdFromRegister(void);
			void ReadVersionFromRegister(void);
			void WriteVersionInRegister();
			void ReadInitializedValueFromRegister(void);
			void WriteInitializedValueInRegister(void);
			void Poll(void);
			void InitChildControllerPins(void);
			int  ReadAveragePinValue(int pinValue, int nbIteration);
			bool ReadPressureInRegister(void);
			bool IsCleanupNeeded(void);
			void ManageStates(void);
			void CheckForPressuresLimits();
			void SetState(int newState);
			void CheckForStateChange(uint8_t locationOfExecution);
			void LogStateChange(void);
			void ProcessAndWriteData(int rawData);
			
			// constantes
			const int ledPin						=  LED_BUILTIN;
			const int PressurePin					= A1;
			const int PinAnalogMode					= 31;
			const int AnalogPrecisionInput			= 8191;
			const int BuiltInHighInputCountDropV	= 235;
			const int BuiltInHighInputCountDropI	= 420;
			
			static const uint8_t CHILD_NEED_PARENT	= 0;
			static const uint8_t CHILD_STAND_ALONE	= 1;
			static const uint8_t CHILD_HYBRID    	= 2;

		public:
			// constructeurs
			Filter_Fusion0x47(int pos, int id, Nextion_Fusion0x47* nextion, Logger_Fusion0x47* logger, Register_Fusion0x47* registers);
			Filter_Fusion0x47(char* codeVersion, Nextion_Fusion0x47* nextion, Logger_Fusion0x47* logger, Register_Fusion0x47* registers);
			
			// objets
			FilterData    Data;

			// fonctions
			
			void InitFilterJob(void);
			void RoutineForChildController(void);
			void ReadPressureOnPin(void);
			void AuthorizeSoundPlay(void);
			void ExecuteRoutineFromParent();
			void LogPressuresData(void);
			void SetCleanUpAtStartup(int value);
			void SetTimeBetweenRead(int timeBetweenRead);
			void SetTempsMsOverMaxPressure(int tempsMsOverMaxPressure);
			void SetMaxFilterPressureAbsolute(int maxFilterPressureAbsolute);
			void SetTimeGapAfterFanStart(int timeGapAfterFanStart);
			void SetTimeGapAfterFanStop(int timeGapAfterFanStop);
			void SetTimeGapAfterSound(int timeGapAfterSound);
			void SetCorrectionInputPressure(int correctionInputPressure);
			void SetMaxPressureTrailingStop(int maxPressureTrailingStop);
			void SetMaxFilterPressure(int maxFilterPressure);
			void SetSensorMaxH20(int sensorMaxH20);
			void SetSound(int soundId);
			void SetChildMode(int childMode);
			void SetShowOnScreen(int isPressureIsShownOnScreen);

	};
#endif /* FILTER_FUSION0X47_H_ */