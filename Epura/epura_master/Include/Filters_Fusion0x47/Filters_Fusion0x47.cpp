#include "Filters_Fusion0x47.h"

Filters_Fusion0x47::Filters_Fusion0x47(){}
	
Filters_Fusion0x47::Filters_Fusion0x47(Nextion_Fusion0x47* nextion, Logger_Fusion0x47* logger, Register_Fusion0x47* registers){
	_Nextion = nextion;
	_Logger = logger;
	_Register = registers;
	Count = 0;
}

void Filters_Fusion0x47::AddFilter(Param_Fusion0x47* param){
	_Filter[Count] = new Filter_Fusion0x47(Count, param->FilterId, _Nextion, _Logger, _Register);
	_Nextion->AddFilter(&(_Filter[Count]->Data));
	_Register->Write(&(_Filter[Count]->Data), _Register->SyncState_Address, _Register->SYNC_ASKED);
	Count++;
}

Filter_Fusion0x47* Filters_Fusion0x47::FindFilterById(int filterId){
	for(int filterPos = 0; filterPos < Count; filterPos++)
		if (_Filter[filterPos]->Data.Id==filterId)
			return(_Filter[filterPos]);
}

void Filters_Fusion0x47::ExecuteRoutineForAllFilters(){
	bool isItTimeToWriteInLog         = (millis() - _LastTimePressuresWasWrite >= _DelayBetweenLog);

	for (int filterPos = 0; filterPos < Count; filterPos++)
		_Filter[filterPos]->ExecuteRoutineFromParent();

	_Nextion->UpdatePressureData();
	if (isItTimeToWriteInLog){
		ThisFilter->LogPressuresData();
		for (int filterPos = 0; filterPos < Count; filterPos++)
			_Filter[filterPos]->LogPressuresData();
	}

	if (isItTimeToWriteInLog)           _LastTimePressuresWasWrite = millis();
}

void Filters_Fusion0x47::ManageSoundPlaying(){
	if (_FilterIdPlayingSound) {
		FilterData* filterPlayingSound = &(FindFilterById(_FilterIdPlayingSound)->Data);
		if (filterPlayingSound->ActualState != filterPlayingSound->STATE_CLEANUP_ASKED &&
			filterPlayingSound->ActualState != filterPlayingSound->STATE_PLAYING_SOUND) {
			_FilterIdPlayingSound = 0;
		}
		if (_Logger->IsEnabledForLevel(1)) sprintf(_Logger->Message, "FilterIdPlayingSound=%u", _FilterIdPlayingSound);
		_Logger->Information(1, filterPlayingSound->Id);
	}
	else{
		for (int pos = 0; pos < Count; pos++){
			if(_Filter[pos]->Data.ActualState == _Filter[pos]->Data.STATE_CLEANUP_ASKED){
				_Filter[pos]->AuthorizeSoundPlay();
				_FilterIdPlayingSound = _Filter[pos]->Data.Id;
				break;
			}
		}
	}
}

void Filters_Fusion0x47::SetDelayBetweenLog(int delayBetweenLog){ _DelayBetweenLog = delayBetweenLog;}
	
