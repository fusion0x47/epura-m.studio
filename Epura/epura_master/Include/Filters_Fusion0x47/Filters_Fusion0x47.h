#ifndef FILTERS_FUSION0X47_H_
	#define FILTERS_FUSION0X47_H_

	#include "../Filter_Fusion0x47/Filter_Fusion0x47.h"
	#include "../Logger_Fusion0x47/Logger_Fusion0x47.h"
	#include "../Nextion_Fusion0x47/Nextion_Fusion0x47.h"
	#include "../param_Fusion0x47/Param_Fusion0x47.h"
	#include "../Register_Fusion0x47/Register_Fusion0x47.h"

	class Filters_Fusion0x47{
		private:
			// variables
			int				_FilterIdPlayingSound = 0;
			unsigned long	_LastTimePressuresWasWrite = 0;
			int				_DelayBetweenLog = 0;

			// objets
			Nextion_Fusion0x47*		_Nextion;
			Logger_Fusion0x47*		_Logger;
			Register_Fusion0x47*	_Register;
			Filter_Fusion0x47*		_Filter[10];

		public:
			// variables
			int Count;

			// objets
			Filter_Fusion0x47*		ThisFilter;
	
			// constructeurs
			Filters_Fusion0x47();
			Filters_Fusion0x47(Nextion_Fusion0x47* nextion, Logger_Fusion0x47* logger, Register_Fusion0x47* registers);

			// fonctions
			void				AddFilter(Param_Fusion0x47* param);
			Filter_Fusion0x47*	FindFilterById(int filterId);
			void				ExecuteRoutineForAllFilters();
			void				ManageSoundPlaying(void);
			void				SetDelayBetweenLog(int delayBetweenLog);
	};
#endif /* FILTERS_FUSION0X47_H_ */