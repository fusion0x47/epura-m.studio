#include "Logger_Fusion0x47.h"

const int INFO = 0;
const int WARN = 1;
const int ERR  = 2;

Logger_Fusion0x47::Logger_Fusion0x47(RTCZero* rtc) {
	_DefaultControllerId			= -1;
	_RestrictLogToChildControllerId	= -1;
	_DayOfLastLog			= -1;
	_RtcUpdated				= false;
	_Rtc				= rtc;
	_EasyNextion = new EasyNex(Serial2);
	_EasyNextion->begin(115200);
}

RTCZero* Logger_Fusion0x47::GetRtc(void){return(_Rtc);}

void Logger_Fusion0x47::SetDefaultValue(bool thereIsAScreen, int defaultControllerId){
	_ThereIsAScreen = thereIsAScreen;
	_DefaultControllerId = defaultControllerId;
}

bool Logger_Fusion0x47::EnableLevelsForSerial(int debugLevelToActivate1, int debugLevelToActivate2, int debugLevelToActivate3, int debugLevelToActivate4, int debugLevelToActivate5, int debugLevelToActivate6, int debugLevelToActivate7, int debugLevelToActivate8, int debugLevelToActivate9, int debugLevelToActivate10){
	if (debugLevelToActivate1 != -1) _DebugOnSerial[debugLevelToActivate1] = 1;
	if (debugLevelToActivate2 != -1) _DebugOnSerial[debugLevelToActivate2] = 1;
	if (debugLevelToActivate3 != -1) _DebugOnSerial[debugLevelToActivate3] = 1;
	if (debugLevelToActivate4 != -1) _DebugOnSerial[debugLevelToActivate4] = 1;
	if (debugLevelToActivate5 != -1) _DebugOnSerial[debugLevelToActivate5] = 1;
	if (debugLevelToActivate6 != -1) _DebugOnSerial[debugLevelToActivate6] = 1;
	if (debugLevelToActivate7 != -1) _DebugOnSerial[debugLevelToActivate7] = 1;
	if (debugLevelToActivate8 != -1) _DebugOnSerial[debugLevelToActivate8] = 1;
	if (debugLevelToActivate9 != -1) _DebugOnSerial[debugLevelToActivate9] = 1;
	if (debugLevelToActivate10 != -1) _DebugOnSerial[debugLevelToActivate10] = 1;
}

bool Logger_Fusion0x47::EnableLevelsForSDCard(int debugLevelToActivate1, int debugLevelToActivate2, int debugLevelToActivate3, int debugLevelToActivate4, int debugLevelToActivate5, int debugLevelToActivate6, int debugLevelToActivate7, int debugLevelToActivate8, int debugLevelToActivate9, int debugLevelToActivate10){
	if (debugLevelToActivate1 != -1) _DebugOnSdCard[debugLevelToActivate1] = 1;
	if (debugLevelToActivate2 != -1) _DebugOnSdCard[debugLevelToActivate2] = 1;
	if (debugLevelToActivate3 != -1) _DebugOnSdCard[debugLevelToActivate3] = 1;
	if (debugLevelToActivate4 != -1) _DebugOnSdCard[debugLevelToActivate4] = 1;
	if (debugLevelToActivate5 != -1) _DebugOnSdCard[debugLevelToActivate5] = 1;
	if (debugLevelToActivate6 != -1) _DebugOnSdCard[debugLevelToActivate6] = 1;
	if (debugLevelToActivate7 != -1) _DebugOnSdCard[debugLevelToActivate7] = 1;
	if (debugLevelToActivate8 != -1) _DebugOnSdCard[debugLevelToActivate8] = 1;
	if (debugLevelToActivate9 != -1) _DebugOnSdCard[debugLevelToActivate9] = 1;
	if (debugLevelToActivate10 != -1) _DebugOnSdCard[debugLevelToActivate10] = 1;
}

void Logger_Fusion0x47::UpdateRTC(RTCZero* rtc) {
	_Rtc = rtc;
	Serial.println(_Rtc->getYear());
	Serial.println(_Rtc->getYear());
	uint8_t e = _Rtc->getYear();
	_RtcUpdated = true;
}

void Logger_Fusion0x47::UpdateDateInStrings() {
	if (_DayOfLastLog != _Rtc->getDay()) {
		if (_RtcUpdated) {
			_DayOfLastLog = _Rtc->getDay();
			char year[4]; sprintf(year, "20%d", _Rtc->getYear());

			char month[2];
			char temp[10];

			if (_Rtc->getMonth() < 10)      sprintf(month, "0%d", _Rtc->getMonth());
			else                          sprintf(month, "%d", _Rtc->getMonth());

			char day[2];
			if (_Rtc->getDay() < 10)        sprintf(day, "0%d", _Rtc->getDay());
			else                          sprintf(day, "%d", _Rtc->getDay());

			sprintf(_DateWithoutSlash, "%c%c%s%s", year[2], year[3], month, day);
			sprintf(_DateWithSlash,    "%s/%s/%s", year, month, day);
		}
		else {
			sprintf(_DateWithoutSlash, "765432");
			sprintf(_DateWithSlash, "9876/54/32");
		}
	}
}

char* Logger_Fusion0x47::GetHeaderOfLine(int criticalityLevel, int debugId, int slaveId) {

	static char headOfLineText [36];
	char criticalityLevelName[14];
	switch (criticalityLevel) {
		case INFO:  sprintf(criticalityLevelName, "Information"); break;
		case WARN:  sprintf(criticalityLevelName, "Warning"); break;
		case ERR:   sprintf(criticalityLevelName, "Error"); break;
	}
	
	char hours[2];
	if (_Rtc->getHours() < 10)      sprintf(hours, "0%d", _Rtc->getHours());
	else                          sprintf(hours, "%d", _Rtc->getHours());

	char minutes[2];
	if (_Rtc->getMinutes() < 10)    sprintf(minutes, "0%d", _Rtc->getMinutes());
	else                          sprintf(minutes, "%d", _Rtc->getMinutes());

	char seconds[2];
	if (_Rtc->getSeconds() < 10)    sprintf(seconds, "0%d", _Rtc->getSeconds());
	else                          sprintf(seconds, "%d", _Rtc->getSeconds());

	UpdateDateInStrings();

	sprintf(headOfLineText, "%s %s:%s:%s,%s,%d, %d", _DateWithSlash, hours, minutes, seconds, criticalityLevelName, debugId, slaveId);

	return (headOfLineText);
}

bool Logger_Fusion0x47::WriteSdCardLogger(char* messageToWrite) {
	if (_DebugOnSerial[4]) {
		char messageForSDCard[250];
		sprintf(messageForSDCard, "WriteSdCardLogger(%s)", messageToWrite);
		Serial.println(messageForSDCard);
	}

	char fileDate[20];
	bool ValWritten = false;

	UpdateDateInStrings();

	//NOUS AVONS LE NOM DU FICHIER CORRESPONDANT A LA DATE, MAINTENANT VERIFIER SI EXISTE

	sprintf(fileDate, "/datalog/%s.csv", _DateWithoutSlash);

	File datalog = SD.open(fileDate, FILE_WRITE);

	if (datalog) { //ECRITURE DU STRING GET POUR LES VARIABLES STOCK�
		datalog.println(messageToWrite);
		ValWritten = true;
		datalog.close();
	}
	else {
		if (_DebugOnSerial[4]) {
			char messageForSDCard[250];
			sprintf(messageForSDCard, "Incapable d'ouvrir le fichier (%s): ERR1011", fileDate);
			Serial.println(messageForSDCard);
		}
	}

	return (ValWritten);
}

void Logger_Fusion0x47::LogOnNextion(int criticalityLevel, char* messageToDisplay){

	switch(criticalityLevel){
		case WARN: 
			_EasyNextion->writeNum("param.ErrWarnColor.val", YellowColor); 
			break;
		case ERR : 	_EasyNextion->writeNum("param.ErrWarnColor.val", RedColor);    break;
	}
	_EasyNextion->writeStr("vis imgCO2,0");
	_EasyNextion->writeStr("vis gaugeCO2,0");
	_EasyNextion->writeStr("vis txtErrAndWarn,1");

	_EasyNextion->writeStr("ErrWarnTxt.txt", messageToDisplay);
	_EasyNextion->writeNum("tmEraseMsg.en", 1);
}


void Logger_Fusion0x47::LogOnSerialAndSDCard(int debugId, int criticalityLevel, int slaveId, char* data ) {
	if (data == "") data = Message;
	if (_RestrictLogToChildControllerId == -1 || _RestrictLogToChildControllerId == slaveId) {
		char lineToWrite[250];
		if (_DebugOnSerial[debugId] || _DebugOnSdCard[debugId] ||
		_DebugOnSerial[0]       || _DebugOnSdCard[0] ) {

			sprintf(lineToWrite, "%s,%s", GetHeaderOfLine(criticalityLevel, debugId, slaveId), data);

			if (_DebugOnSerial[debugId] || _DebugOnSerial[0]) Serial.println(lineToWrite);
			if (_DebugOnSdCard[debugId] || _DebugOnSdCard[0]) WriteSdCardLogger(lineToWrite);
		}
	}
}

void Logger_Fusion0x47::Information(int debugId, int slaveId, char* data)	
	{	LogOnSerialAndSDCard(debugId, INFO, slaveId, data); }
		
void Logger_Fusion0x47::Information(int debugId, char* data)				
	{	LogOnSerialAndSDCard(debugId, INFO, _DefaultControllerId, data); }

void Logger_Fusion0x47::Warning(int debugId, int errId, char* explanation, char* messageToDisplay){	
	Warning(debugId, errId, _DefaultControllerId, explanation, messageToDisplay);
}
		
void Logger_Fusion0x47::Warning(int debugId, int errId, int slaveId, char* explanation, char* messageToDisplay){
	LogOnSerialAndSDCard(debugId, WARN, slaveId, explanation);
	if(_ThereIsAScreen){
		if(messageToDisplay == ""){	LogOnNextion(WARN, explanation); }
		else						LogOnNextion(WARN, messageToDisplay);
		delay(3000);
	}
}
	
void Logger_Fusion0x47::Error(int debugId, int errId, char* explanation, char* messageToDisplay){
	Error(debugId, errId, _DefaultControllerId, explanation, messageToDisplay);
}
		
void Logger_Fusion0x47::Error(int debugId, int errId, int slaveId, char* explanation, char* messageToDisplay){
	LogOnSerialAndSDCard(debugId, ERR, _DefaultControllerId, explanation);
	if(_ThereIsAScreen){
		
		if(messageToDisplay == "")	LogOnNextion(ERR, explanation);
		else						LogOnNextion(ERR, messageToDisplay);
		delay(5000);
	}
	NVIC_SystemReset();
}


bool Logger_Fusion0x47::IsEnabledForLevel(int debugLevelToActivate) {
	return (_DebugOnSerial[debugLevelToActivate] || _DebugOnSdCard[debugLevelToActivate] || _DebugOnSerial[0] || _DebugOnSdCard[0] );
}



