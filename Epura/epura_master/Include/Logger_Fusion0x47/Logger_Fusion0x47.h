#ifndef LOGGER_H_
	#define LOGGER_H_

	#include <stdio.h>
	#include <stdlib.h>
	#include <math.h>
	#include "../RTCZero/RTCZero.h"
	#include "../SD/SD.h"
	#include "../EasyNextion/EasyNextionLibrary.h"


	class Logger_Fusion0x47 {
		private:
			// variables
			uint8_t _DefaultControllerId;
			uint8_t _ThereIsAScreen;
			char _DateWithSlash[20];
			char _DateWithoutSlash[20];
			int  _DayOfLastLog;
			bool _RtcUpdated;
			bool _DebugOnSerial[10]={0,0,0,0,0,0,0,0,0,0};
			bool _DebugOnSdCard[10]={0,0,0,0,0,0,1,0,0,0};
			int  _RestrictLogToChildControllerId;
	
			// objets
			RTCZero* _Rtc;
			EasyNex* _EasyNextion;

			// fonctions
			void	LogOnNextion(int criticalityLevel, char* displayMessage);
			void	LogOnSerialAndSDCard(int debugId, int criticalityLevel, int slaveId, char* data = "");
			char*	GetHeaderOfLine(int criticalityLevel, int debugId, int slaveId);
			void	UpdateDateInStrings(void);
			bool	WriteSdCardLogger(char* messageToWrite);

			static const uint32_t YellowColor = 65504;
			static const uint32_t RedColor    = 63488;

		public:
			// variables
			char Message[250];
			char MessageForScreen[50];
		
			// constructeurs
			Logger_Fusion0x47(RTCZero* rtc);

			// fonctions
			RTCZero* GetRtc(void);
			void SetDefaultValue(bool thereIsAScreen, int defaultControllerId);
			bool IsEnabledForLevel(int debugLevelId);
			bool EnableLevelsForSDCard(int debugLevelToActivate1 = -1, int debugLevelToActivate2 = -1, int debugLevelToActivate3 = -1, int debugLevelToActivate4 = -1, int debugLevelToActivate5 = -1, int debugLevelToActivate6 = -1, int debugLevelToActivate7 = -1, int debugLevelToActivate8 = -1, int debugLevelToActivate9 = -1, int debugLevelToActivate10 = -1);
			bool EnableLevelsForSerial(int debugLevelToActivate1 = -1, int debugLevelToActivate2 = -1, int debugLevelToActivate3 = -1, int debugLevelToActivate4 = -1, int debugLevelToActivate5 = -1, int debugLevelToActivate6 = -1, int debugLevelToActivate7 = -1, int debugLevelToActivate8 = -1, int debugLevelToActivate9 = -1, int debugLevelToActivate10 = -1);
			void UpdateRTC(RTCZero* rtc);
			void Information(int debugId, int slaveId, char* data = "");
			void Information(int debugId, char* data = "");
			void Warning(int debugId, int slaveId, int errId, char* explanation, char* messageToDisplay = "");
			void Warning(int debugId, int errId, char* explanation, char* messageToDisplay = "");
			void Error(int debugId, int slaveId, int errId, char* explanation, char* messageToDisplay = "");
			void Error(int debugId, int errId, char* explanation, char* messageToDisplay = "");
	};
#endif /* LOGGER_H_ */

