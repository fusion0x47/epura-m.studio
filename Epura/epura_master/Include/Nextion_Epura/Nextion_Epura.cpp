#include "Nextion_Epura.h"

const int	CABIN_IMG_ID = 3;
const int	ENGIN_IMG_ID = 7;
const char  NEXTION_UPDATE_FILENAME[11] = "update.tft";
const int	_chipSelect_SD = SDCARD_SS_PIN;
NexUpload   NextionUploading(NEXTION_UPDATE_FILENAME, _chipSelect_SD, 115200);

Nextion_Epura::Nextion_Epura(Logger_Epura* logger, Register_Epura* registers){
	delay(100);
	_Logger=logger;
	_EasyNextion = new EasyNex(Serial2);
	_EasyNextion->begin(115200);
	_EasyNextion->writeStr("page Loading");
	_Register = registers;
}

Nextion_Epura::Nextion_Epura(){
	int a =1;
}

void Nextion_Epura::Init(RTCZero* rtc, char* versionParent){
	UpdateRtcWithNextion(rtc);

	char commandForNextion[40];
	int posOnScreen = 1;
	for(int noPcb = 0; noPcb < _NbPcb; noPcb++){
		if(_PcbData[noPcb]->ShowDataOnScreen){
			sprintf(commandForNextion, "param.showF%d.val=1", posOnScreen);
			_EasyNextion->writeStr(commandForNextion);
			
			int imgId = (_PcbData[noPcb]->Type == CABIN_TYPE) ? CABIN_IMG_ID : ENGIN_IMG_ID;
			sprintf(commandForNextion, "main.imgF%d.pic=%d", posOnScreen, imgId);
			_EasyNextion->writeStr(commandForNextion);
			
			if(_PcbData[noPcb]->Id == 0)				sprintf(commandForNextion, "param.showBtnF%d.val=0", posOnScreen);	
			else											sprintf(commandForNextion, "param.showBtnF%d.val=1", posOnScreen);	
			_EasyNextion->writeStr(commandForNextion);
			
			_PcbData[noPcb]->PosOnScreen = posOnScreen;
			posOnScreen++;
		}
	}
	
	for(int posPcb = posOnScreen; posPcb < 4; posPcb++){
		sprintf(commandForNextion, "param.showF%d.val=0", posPcb);
		_EasyNextion->writeStr(commandForNextion);
	}		
	
	sprintf(commandForNextion, "param.VersionParent.txt=\"%s\"", versionParent);
	_EasyNextion->writeStr(commandForNextion);

	_EasyNextion->writeStr("page main");
}

void Nextion_Epura::SetEasyNextion(EasyNex *easyNextion){
	_EasyNextion = easyNextion;
}

void Nextion_Epura::Write(char* nextionElement, int elementValue)   {_EasyNextion->writeNum(nextionElement, elementValue);}

void Nextion_Epura::Write(char* nextionElement, char* elementValue) {_EasyNextion->writeStr(nextionElement, elementValue);}

uint32_t Nextion_Epura::ReadUInt(char* nextionElement)              {return(_EasyNextion->readNumber(nextionElement));}

void Nextion_Epura::AddPcb(PcbData* filterData){
	_PcbData[_NbPcb] = filterData;
	_NbPcb++;
}

void Nextion_Epura::ManageUpdate(){
	if(SD.exists(NEXTION_UPDATE_FILENAME)){
		unsigned int millisBeforeUpload = millis();
		NextionUploading.upload();
		if (millis() - millisBeforeUpload > 5000){
			SD.remove("update.tft");
			_Logger->Information(6, 0, "mise-�-jour de l'�cran r�ussie!");
		}
		else{
			_Logger->Information(6, 0, "�chec de la mise-�-jour de l'�cran!");
		}

	}
}

void Nextion_Epura::UpdateRtcWithNextion(RTCZero* rtcToUpdate) {

	uint32_t RtcAnnee    = 0;
	uint32_t RtcMois     = 0;
	uint32_t RtcJour     = 0;
	uint32_t RtcHeure    = 0;
	uint32_t RtcMinute   = 0;
	uint32_t RtcSeconde  = 0;



	if (_Logger->IsEnabledForLevel(1))sprintf(_Logger->Message, "Date et heure avant modification dans RTC:  %d/%d/%d %d:%d:%d",
																rtcToUpdate->getYear(), rtcToUpdate->getMonth(), rtcToUpdate->getDay(),
																rtcToUpdate->getHours(), rtcToUpdate->getMinutes(), rtcToUpdate->getSeconds());
	_Logger->Information(1, 0);

	RtcAnnee   = _EasyNextion->readNumber("param.Year.val");
	RtcAnnee   = _EasyNextion->readNumber("param.Year.val");
	RtcMois    = _EasyNextion->readNumber("param.Month.val");
	RtcJour    = _EasyNextion->readNumber("param.Day.val");
	RtcHeure   = _EasyNextion->readNumber("param.Hour.val");
	RtcMinute  = _EasyNextion->readNumber("param.Minut.val");
	RtcSeconde = _EasyNextion->readNumber("param.Second.val");

	if (_Logger->IsEnabledForLevel(1))sprintf(_Logger->Message, "Date et heure dans Nextion:  %d/%d/%d %d:%d:%d",
																							RtcAnnee, RtcMois, RtcJour,
																							RtcHeure, RtcMinute, RtcSeconde);
	_Logger->Information(1, 0);

	rtcToUpdate->setYear(RtcAnnee); // rtcToUpdate->setYear(RtcAnnee); ANNEE DANS NEXTION EST EN TERME DE 2000, ET EN TERME DE 20 DANS SAMD.
	rtcToUpdate->setMonth(RtcMois);
	rtcToUpdate->setDay(RtcJour);
	rtcToUpdate->setHours(RtcHeure);
	rtcToUpdate->setMinutes(RtcMinute);
	rtcToUpdate->setSeconds(RtcSeconde);

	if (_Logger->IsEnabledForLevel(1))sprintf(_Logger->Message, "Date et heure apr�s modification:  %d/%d/%d %d:%d:%d",
	rtcToUpdate->getYear(), rtcToUpdate->getMonth(), rtcToUpdate->getDay(),
	rtcToUpdate->getHours(), rtcToUpdate->getMinutes(), rtcToUpdate->getSeconds());
	_Logger->Information(1, 0);

	if (rtcToUpdate->getYear() > 0)  _Logger->UpdateRTC(rtcToUpdate);

	_Logger->Information(7, 0, "Startup events, UPDATE RTC Done");
	_Logger->Information(6, 0, "Etat: Heure ajuste avec l'ecran,I103");

}

void Nextion_Epura::UpdatePressureData() {
	unsigned long UpdatePressureDataOnNextionTime = millis();
	_Logger->Information(9, 0, "function, UpdatePressureData()");

	int gaugeValue;
	char nextionCommand[32];
	if (millis() - _LastTimmeDataWereSendToNextion > _DelayBetweenDisplay) {
		for(int noPcb = 0; noPcb < _NbPcb; noPcb++){
			if(_PcbData[noPcb]->ShowDataOnScreen){
				sprintf(nextionCommand, "param.PressureF%d.txt=\"%5.2f\"",  _PcbData[noPcb]->PosOnScreen,  _PcbData[noPcb]->LiveFilterPressure);
				_EasyNextion->writeStr(nextionCommand);

				gaugeValue = map( _PcbData[noPcb]->LiveFilterPressure, 0,  _PcbData[noPcb]->AbsoluteMaxPressure, 0, 135);
				gaugeValue = (gaugeValue > 180) ? gaugeValue = 180 : gaugeValue = (gaugeValue < 0) ? gaugeValue = 0 : gaugeValue = gaugeValue ;
				sprintf(nextionCommand, "main.gaugeF%d.val=%d",  _PcbData[noPcb]->PosOnScreen, gaugeValue);
				_EasyNextion->writeStr(nextionCommand);
				
			}
		}
		UpdateUptime();
		_LastTimmeDataWereSendToNextion = millis();
	}
	sprintf(_Logger->Message, "Time to execute UpdatePressureDataOnNextionTime,%u milliseconds", millis() - UpdatePressureDataOnNextionTime);
	_Logger->Information(3, 0);
}

void Nextion_Epura::UpdateUptime(){
	unsigned long timeToAddToUptime = millis() - _LastTimmeDataWereSendToNextion;
	char commandToNextion[40];
	sprintf(commandToNextion, "paramInfo=param.UpTime.val+%d", timeToAddToUptime);
	sprintf(commandToNextion, "wepo UpTime, 0");
}

void Nextion_Epura::SetChildControllerState(PcbData* pcbData) {
	sprintf(_Logger->Message, "function, SetChildControllerStateOnNextion(filterId=%d)", pcbData->Id);
	_Logger->Information(9, pcbData->Id);

	char MessageOnButton[25];
	uint32_t bgColor;

	switch (pcbData->ActualState) {
		case pcbData->STATE_NOT_READY:
			sprintf(MessageOnButton, "-");
			bgColor = YellowColor;
			break;

		case pcbData->STATE_READYINLOOP:
		case pcbData->STATE_START_FAN:
			sprintf(MessageOnButton, "OK");
			bgColor = GreenColor;
			break;

		case pcbData->STATE_READYINLOOP_NEED_CLEANUP:
			sprintf(MessageOnButton, "Nettoyage\\rrequis");
			bgColor = YellowColor;
			break;

		case pcbData->STATE_READYINLOOP_CLEANUP_ALERT:
			sprintf(MessageOnButton, "Nettoyage\\rObligatoire");
			bgColor = RedColor;
			break;

		case pcbData->STATE_CLEANUP_ASKED:
		case pcbData->STATE_PLAYING_SOUND:
			sprintf(MessageOnButton, "Nettoyage\\ren cours");
			bgColor = YellowColor;
			break;

		case pcbData->STATE_CHANGEFILTER:
			sprintf(MessageOnButton, "Changer\\rfiltre");
			bgColor = RedColor;
			break;

	}

	char buttonToModify[6];	sprintf(buttonToModify, "btnF%d", pcbData->PosOnScreen);

	char nextionCommand[20];	
	
	sprintf(nextionCommand, "%s.txt=\"%s\"", buttonToModify, MessageOnButton);
	_EasyNextion->writeStr(nextionCommand);

	sprintf(nextionCommand, "%s.bco=%d", buttonToModify, bgColor);
	_EasyNextion->writeStr(nextionCommand);
	
}

int Nextion_Epura::ReceiveInteractionFromScreen() {
	unsigned long receiveInteractionFromScreenStartTime = millis();
	_Logger->Information(9, 0, "function, ReceiveInteractionFromScreen()");

	if (millis() - _LastTimeDataReceivedFromNextion > 500) { //�vite le double clique par erreur
		for (int filterPos = 0; filterPos < _NbPcb; filterPos++){
			if (_PcbData[filterPos]->ShowDataOnScreen){
				uint32_t Action = 0;
				char paramName[20];
				sprintf(paramName, "param.Filter%dClnAsk.val", filterPos + 1);
				if (ReadUInt(paramName)) {
					if (_PcbData[filterPos]->Type == ENGIN_TYPE){
						_Logger->Information(1, _PcbData[filterPos]->Id, "Interaction from screen, Motor filter cleanup asked");
						_Logger->Information(6, _PcbData[filterPos]->Id, "Etat: Nettoyage manuel moteur demande,I201");
					}
					else{
						_Logger->Information(1, _PcbData[filterPos]->Id, "Interaction from screen, Cabin filter cleanup asked");
						_Logger->Information(6, _PcbData[filterPos]->Id, "Etat: Nettoyage manuel cabine demande,I202");
					}
					_PcbData[filterPos]->ActualState = _PcbData[filterPos]->STATE_CLEANUP_ASKED;
					SetChildControllerState(_PcbData[filterPos]);
					_Register->Write(_PcbData[filterPos], ChildControllerStateAddress, _PcbData[filterPos]->STATE_CLEANUP_ASKED);
					_Logger->Information(1, _PcbData[filterPos]->Id, "ChildController state, STATE_CLEANUP_ASKED (3)");
					_Logger->Information(6, _PcbData[filterPos]->Id, "Etat: Nettoyage demande,I3");
					Write(paramName, 0);
			
				}
			}
		}
		_LastTimeDataReceivedFromNextion = millis();
	}
	sprintf(_Logger->Message, "Time to execute ReceiveInteractionFromScreen,%u milliseconds", millis() - receiveInteractionFromScreenStartTime);
	_Logger->Information(3, 0);
}

void Nextion_Epura::SetDelayBetweenDisplay(int delayBetweenDisplay){_DelayBetweenDisplay = delayBetweenDisplay;}

void Nextion_Epura::SetDelayForErrorMessage(int delayForErrorMessage){
	Write("param.DelayForErr", delayForErrorMessage);
}
