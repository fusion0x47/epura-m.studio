#ifndef NEXTION_EPURA_H_
	#define NEXTION_EPURA_H_

	#include "../EasyNextion/EasyNextionLibrary.h"
	#include "../Nextion/NexUpload.h"
	#include "../Logger_Epura/Logger_Epura.h"
	#include "../Pcb_Epura/Pcb_Epura.h"
	#include "../Register_Epura/Register_Epura.h"
	#include "wiring_private.h"

	static const uint32_t GreenColor  = 2016;
	static const uint32_t YellowColor = 65504;
	static const uint32_t RedColor    = 63488;
	static const uint32_t WhiteColor  = 65535;
	static const uint32_t BrownColor  = 33280;
	static const uint32_t EpuraColor  = 1387;

	class Nextion_Epura {

		private:
			// variables
			unsigned long		_TimerReceiveNextion = 0;
			int					_NbPcb = 0;
			unsigned long		_LastTimeDataReceivedFromNextion = 0;
			unsigned long		_LastTimmeDataWereSendToNextion;
			int					_DelayBetweenDisplay;
			
			// objets
			Logger_Epura*		_Logger;
			EasyNex*	     		_EasyNextion;
			Register_Epura*	_Register;
			PcbData*				_PcbData[4];

			// fonctions
			void HidePcbOnScreen(int noPcb);
			void PosPcbOnScreen(int noPcb, int yPos);
			void UpdateUptime(void);
		
		public:
			// constructeurs
			Nextion_Epura(void);
			Nextion_Epura(Logger_Epura* logger, Register_Epura* registers);
			
			// fonctions
			void		Init(RTCZero* rtc, char* versionParent);
			void		AddPcb(PcbData* pcbData);
			void		SetEasyNextion(EasyNex *easyNextion);
			void		SetTextOn2Lines(void);
			void		SetDelayBetweenDisplay(int delayBetweenDisplay);
			void		SetDelayForErrorMessage(int delayForErrorMessage);
			void		Write(char* NextionElement, int elementValue);
			void		Write(char* NextionElement, char* elementValue);
			uint32_t	ReadUInt(char* NextionElement);
			void		ManageUpdate(void);
			void		SetChildControllerState(PcbData* pcb);
			void		UpdatePressureData(void);
			int			ReceiveInteractionFromScreen(void);
			void		UpdateRtcWithNextion(RTCZero* rtcToUpdate);
	};
#endif /* NEXTION_EPURA_H_ */