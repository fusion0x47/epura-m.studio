#ifndef NEXTION_FUSION0X47_H_
	#define NEXTION_FUSION0X47_H_

	#include "../EasyNextion/EasyNextionLibrary.h"
	#include "../Nextion/NexUpload.h"
	#include "../Logger_Fusion0x47/Logger_Fusion0x47.h"
	#include "../Filter_Fusion0x47/FilterData.h"
	#include "../Register_Fusion0x47/Register_Fusion0x47.h"
	#include "wiring_private.h"

	class Nextion_Fusion0x47 {

		private:
			// variables
			unsigned long		_TimerReceiveNextion = 0;
			int					_NbFilter = 0;
			unsigned long		_LastTimeDataReceivedFromNextion = 0;
			unsigned long		_LastTimmeDataWereSendToNextion;
			int					_DelayBetweenDisplay;
			
			// objets
			Logger_Fusion0x47*		_Logger;
			EasyNex*	     		_EasyNextion;
			Register_Fusion0x47*	_Register;
			FilterData*				_FilterData[4];

			// fonctions
			void HideFilterOnScreen(int noFilter);
			void PosFilterOnScreen(int noFilter, int yPos);
			void UpdateUptime(void);
		
		public:
			// constructeurs
			Nextion_Fusion0x47(void);
			Nextion_Fusion0x47(Logger_Fusion0x47* logger, Register_Fusion0x47* registers);
			
			// fonctions
			void		Init(RTCZero* rtc, char* versionParent);
			void		AddFilter(FilterData* filterData);
			void		SetEasyNextion(EasyNex *easyNextion);
			void		SetTextOn2Lines(void);
			void		SetDelayBetweenDisplay(int delayBetweenDisplay);
			void		SetDelayForErrorMessage(int delayForErrorMessage);
			void		Write(char* NextionElement, int elementValue);
			void		Write(char* NextionElement, char* elementValue);
			uint32_t	ReadUInt(char* NextionElement);
			void		ManageUpdate(void);
			void		SetChildControllerState(FilterData* filter);
			void		UpdatePressureData(void);
			int			ReceiveInteractionFromScreen(void);
			void		UpdateRtcWithNextion(RTCZero* rtcToUpdate);
			
			// constantes
			
			static const uint32_t GreenColor  = 2016;
			static const uint32_t YellowColor = 65504;
			static const uint32_t RedColor    = 63488;
			static const uint32_t WhiteColor  = 65535;
			static const uint32_t BrownColor  = 33280;
			static const uint32_t EpuraColor  = 1387;
	};
#endif /* NEXTION_FUSION0X47_H_ */