#include "PcbList_Epura.h"

PCBList_Propulsa::PCBList_Propulsa(){}
	
PCBList_Propulsa::PCBList_Propulsa(Nextion_Epura* nextion, Logger_Epura* logger, Register_Epura* registers){
	_Nextion = nextion;
	_Logger = logger;
	_Register = registers;
	Count = 0;
}

void PCBList_Propulsa::AddPcbData(Param_Epura* param){
	_Pcb[Count] = new Pcb_Epura(Count, param->PcbId, _Nextion, _Logger, _Register);
	_Nextion->AddPcb(&(_Pcb[Count]->Data));
	_Register->Write(&(_Pcb[Count]->Data), SyncState_Address, SYNC_ASKED);
	Count++;
}

Pcb_Epura* PCBList_Propulsa::FindPcbById(int filterId){
	for(int filterPos = 0; filterPos < Count; filterPos++)
		if (_Pcb[filterPos]->Data.Id==filterId)
			return(_Pcb[filterPos]);
}

void PCBList_Propulsa::ExecuteRoutineForAllPcbOnList(){
	bool isItTimeToWriteInLog         = (millis() - _LastTimePressuresWasWrite >= _DelayBetweenLog);

	for (int filterPos = 0; filterPos < Count; filterPos++)
		_Pcb[filterPos]->ExecuteRoutineFromParent();

	_Nextion->UpdatePressureData();
	if (isItTimeToWriteInLog){
		ThisPcb->LogPressuresData();
		for (int filterPos = 0; filterPos < Count; filterPos++)
			_Pcb[filterPos]->LogPressuresData();
	}

	if (isItTimeToWriteInLog)           _LastTimePressuresWasWrite = millis();
}

void PCBList_Propulsa::ManageSoundPlaying(){
	if (_PcbIdPlayingSound) {
		PcbData* filterPlayingSound = &(FindPcbById(_PcbIdPlayingSound)->Data);
		if (filterPlayingSound->ActualState != filterPlayingSound->STATE_CLEANUP_ASKED &&
			filterPlayingSound->ActualState != filterPlayingSound->STATE_PLAYING_SOUND) {
			_PcbIdPlayingSound = 0;
		}
		if (_Logger->IsEnabledForLevel(1)) sprintf(_Logger->Message, "PcbIdPlayingSound=%u", _PcbIdPlayingSound);
		_Logger->Information(1, filterPlayingSound->Id);
	}
	else{
		for (int pos = 0; pos < Count; pos++){
			if(_Pcb[pos]->Data.ActualState == _Pcb[pos]->Data.STATE_CLEANUP_ASKED){
				_Pcb[pos]->AuthorizeSoundPlay();
				_PcbIdPlayingSound = _Pcb[pos]->Data.Id;
				break;
			}
		}
	}
}

void PCBList_Propulsa::SetDelayBetweenLog(int delayBetweenLog){ _DelayBetweenLog = delayBetweenLog;}
	
