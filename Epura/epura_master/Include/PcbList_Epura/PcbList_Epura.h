#ifndef PCBList_Propulsa_H_
	#define PCBList_Propulsa_H_

	#include "../Pcb_Epura/Pcb_Epura.h"
	#include "../Logger_Epura/Logger_Epura.h"
	#include "../Nextion_Epura/Nextion_Epura.h"
	#include "../param_Epura/Param_Epura.h"
	#include "../Register_Epura/Register_Epura.h"

	class PCBList_Propulsa{
		private:
			// variables
			int				_PcbIdPlayingSound = 0;
			unsigned long	_LastTimePressuresWasWrite = 0;
			int				_DelayBetweenLog = 0;

			// objets
			Nextion_Epura*		_Nextion;
			Logger_Epura*		_Logger;
			Register_Epura*	_Register;
			Pcb_Epura*		_Pcb[10];

		public:
			// variables
			int Count;

			// objets
			Pcb_Epura*		ThisPcb;
	
			// constructeurs
			PCBList_Propulsa();
			PCBList_Propulsa(Nextion_Epura* nextion, Logger_Epura* logger, Register_Epura* registers);

			// fonctions
			void				AddPcbData(Param_Epura* param);
			Pcb_Epura*			FindPcbById(int filterId);
			void				ExecuteRoutineForAllPcbOnList();
			void				ManageSoundPlaying(void);
			void				SetDelayBetweenLog(int delayBetweenLog);
	};
#endif /* PCBList_Propulsa_H_ */