#ifndef PCBDATA_H_
	#define PCBDATA_H_
	#include <sys\_stdint.h>

	static const uint8_t ENGIN_TYPE							=  1; // Pour Type
	static const uint8_t CABIN_TYPE							=  2; // Pour Type

	static const uint8_t PARENT_CONTROLLER_TYPE				=  1; // Pour ControllerType
	static const uint8_t CHILD_CONTROLLER_TYPE				=  2; // Pour ControllerType

	static const uint8_t STATE_NOT_READY                    =  0;
	static const uint8_t STATE_STOP_FAN						=  1;
	static const uint8_t STATE_CLEANUP_ASKED                =  2;
	static const uint8_t STATE_PLAYING_SOUND                =  3;
	static const uint8_t STATE_START_FAN					=  4;
	static const uint8_t STATE_READYINLOOP                  =  5;
	static const uint8_t STATE_READYINLOOP_NEED_CLEANUP     =  6;
	static const uint8_t STATE_READYINLOOP_CLEANUP_ALERT    =  7;
	static const uint8_t STATE_CHANGEFILTER                 =  8;
	static const uint8_t STATE_UPDATING                     =  9;

	class PcbData{
		public:
			// variables
			int				Pos;
			int				PosOnScreen;
			int				Type;
			int				Id;
			uint8_t			ActualState				= STATE_NOT_READY;
			unsigned long	LastDisplayTime;
			float			LiveFilterPressure;
			int				ControllerType;
			bool			ThereIsScreenOnThisController;
			bool			ShowDataOnScreen;
			float			AbsoluteMaxPressure;
			float			MaxFilterPressure;
			
	};
#endif /* PCBDATA_H_ */