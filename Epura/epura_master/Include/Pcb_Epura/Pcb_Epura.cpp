#include "Pcb_Epura.h"

int Dout1_pin = 8;
int Dout2_pin = 1;
int Dout3_pin = 0;
uint16_t fanPin = Dout2_pin;

const uint8_t EXECUTED_ON_PARENT = 0;
const uint8_t EXECUTED_ON_CHILD  = 1;

// Constructeurs
Pcb_Epura::Pcb_Epura(int pos, int id, Nextion_Epura* nextion, 	Logger_Epura* logger, Register_Epura* registers){ // For Parent
	Data.ControllerType = Data.PARENT_CONTROLLER_TYPE;
	Data.Pos			= pos;
	Data.Id				= id;
	InitObjects(nextion, logger, registers);
}

Pcb_Epura::Pcb_Epura(char* codeVersion, Nextion_Epura* nextion, Logger_Epura* logger, Register_Epura* registers){ // For Child
	Data.Type = CABIN_TYPE;
	Data.Id = 0;
	_CodeVersion		= codeVersion;
	InitObjects(nextion, logger, registers);
	nextion->Write("param.VersionParent.txt", codeVersion);
}

void Pcb_Epura::InitObjects(Nextion_Epura* nextion, Logger_Epura* logger, Register_Epura* registers){ // For Child and Parent
	_Nextion			= nextion;
	_Logger				= logger;
	_Register			= registers;
	_DepartSensor		= (digitalRead(PinAnalogMode) == LOW) ? 1520: 0;
	_Rtc				= _Logger->GetRtc();
}

void Pcb_Epura::RoutineForChildController(){ // For Child
	Poll();
	if (_Register->Read(&Data, SyncState_Address) == SYNC_ASKED)		ReadInitializedValueFromRegister();
	Poll();
	ManageStates();
	Poll();
	ReadPressureOnPin();
	Poll();
}

void Pcb_Epura::ManageStates() { // For Child
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, ManageChildControllerStates(ChildControllerPos=%d)", Data.Pos);
	_Logger->Information(9, Data.Id);

 	switch (Data.ActualState) {
		case Data.STATE_NOT_READY:
			switch(_ChildMode){
				case CHILD_NEED_PARENT:{
					uint16_t actualSyncState = _Register->Read(&Data, SyncState_Address);

					if (actualSyncState == SYNC_NOT_INITIALIZED)
						_Register->Write(&Data, SyncState_Address, SYNC_ASKED);
					else
						if (actualSyncState >= SYNCHED_PARENT_TO_CHILD && _CleanUpAtStartup){
							if (Data.Type == CABIN_TYPE)	SetState(Data.STATE_STOP_FAN);
							else								SetState(Data.STATE_CLEANUP_ASKED);

							_CleanUpAtStartup = false;
						}
						
					break;
				}
				
				case CHILD_STAND_ALONE:
					if (_CleanUpAtStartup){		
						if (Data.Type == CABIN_TYPE)	SetState(Data.STATE_STOP_FAN);
						else								SetState(Data.STATE_CLEANUP_ASKED);

						_CleanUpAtStartup = false;
					}
					break;
			}
			break;
			
		case Data.STATE_STOP_FAN:
			digitalWrite(fanPin, LOW);
			if (millis() - _TimeGapAfterFanStop >= _StateStartTime) {
				SetState(Data.STATE_CLEANUP_ASKED);
				_Logger->Information(6, Data.Id, "Etat: Nettoyage demande,I3");
			}
			break;

		case Data.STATE_CLEANUP_ASKED:
			_IsSoundAuthorized = _Register->Read(&Data, AuthorizedForSound_Address);
			if (_IsSoundAuthorized)						SetState(Data.STATE_PLAYING_SOUND);
			break;
			
		case Data.STATE_PLAYING_SOUND:
			_Audio->Play();
			if (Data.Type == CABIN_TYPE)	SetState(Data.STATE_START_FAN);
			else                                SetState(Data.STATE_READYINLOOP);
			break;

		case Data.STATE_START_FAN:
			digitalWrite(fanPin, HIGH);
			if (millis() - _StateStartTime >= _TimeGapAfterFanStart) {
				if (Data.MaxFilterPressure < Data.LiveFilterPressure + 1)
					Data.MaxFilterPressure = Data.LiveFilterPressure + 1;
				SetState(Data.STATE_READYINLOOP);
			}
			break;

		case Data.STATE_READYINLOOP_NEED_CLEANUP: // l'�tat STATE_READYINLOOP avec avertissement en jaune
		case Data.STATE_READYINLOOP_CLEANUP_ALERT: // l'�tat STATE_READYINLOOP avec avertissement en rouge
		case Data.STATE_READYINLOOP: // l'�tat STATE_READYINLOOP
			CheckForPressuresLimits();
			CheckForStateChange(EXECUTED_ON_CHILD);
			break;
	}
}

void Pcb_Epura::SetState(int newState){ // For Child
	_StateModified = (Data.ActualState != newState);
	if (_StateModified){
		_Register->Write(&Data, ChildControllerStateAddress, newState);
		Data.ActualState = newState;
		LogStateChange();
		_StateStartTime = millis();
	}
}

void Pcb_Epura::CheckForStateChange(uint8_t locationOfExecution){ // For Child and Parent
	int newState = _Register->Read(&Data, ChildControllerStateAddress);

	_StateModified = (Data.ActualState != newState);
	Data.ActualState = newState;

	if (_StateModified){
		if(locationOfExecution == EXECUTED_ON_PARENT){
			_Nextion->SetChildControllerState(&Data);
		}
		_StateStartTime = millis();
		LogStateChange();
	}
	
}

void Pcb_Epura::AuthorizeSoundPlay(){// For Parent
	_IsSoundAuthorized = true;
	_Register->Write(&Data, AuthorizedForSound_Address, 1);
}

void Pcb_Epura::CheckForPressuresLimits() { // For Child
	sprintf(_Logger->Message, "function, CheckForPressuresLimits()");
	_Logger->Information(9, Data.Id);

	if (Data.LiveFilterPressure > Data.MaxFilterPressure) {
		SetState(Data.STATE_CHANGEFILTER);
	}
	else {
		if ((Data.Type == ENGIN_TYPE && Data.LiveFilterPressure >= Data.AbsoluteMaxPressure)) { // S'il est dans le rouge
			Data.ActualState = Data.STATE_READYINLOOP_CLEANUP_ALERT;
			_Logger->Information(6, Data.Id, "Etat: Nettoyage obligatoire,I9");
		}
		else {
			if (IsCleanupNeeded()) {
				Data.ActualState = Data.STATE_READYINLOOP_NEED_CLEANUP;
				_Logger->Information(6, Data.Id, "Etat: Nettoyage neccessaire,I8");
			}
			else{
				SetState(Data.STATE_READYINLOOP);
				_Logger->Information(6, Data.Id, "Etat: en fonctionnement normal,I7");
			}

		}
	}
}

bool Pcb_Epura::IsCleanupNeeded() { // For Child
	if (_Logger->IsEnabledForLevel(9))  sprintf(_Logger->Message, "function, IsCleanupNeeded()");
	_Logger->Information(9, Data.Id);

	if (Data.LiveFilterPressure > Data.MaxFilterPressure
		|| (Data.LiveFilterPressure > (Data.MaxFilterPressure - _MaxPressureTrailingStop) && _TimerOverPressure > 0)) {

		if (_TimerOverPressure) {
			if (millis() - _TimerOverPressure >= _TempsMsOverMaxPressure) {
				_Logger->Information(5, Data.Id, "PCBPropulsa Pressure Over Limit, AskForCleanup");
				return (true);
			}
		}
		else {
			_TimerOverPressure = millis(); //DEMARRAGE DU TIMER POUR QUE LE LAVAGE DEVIENNE NECESSAIRE DANS X SECONDES
		}
	}
	else 
		_TimerOverPressure = 0;

	return (false); //Si nous sommes ici nous retournons false car nous ne sommes pas rendu � faire un lavage
}

bool Pcb_Epura::ReadPressureInRegister() { // For Parent
	unsigned long readPcbInputsTime = millis();
	if (readPcbInputsTime - _LastReadTime < _TimeBetweenRead)  return false;
	
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, ReadPressureInRegister()");
	_Logger->Information(9, Data.Id);

	float mappedRawData;
	int rawData;

	rawData = _Register->Read(&Data, Pressure_Address);

	ProcessAndWriteData(rawData);

	if (_Logger->IsEnabledForLevel(3)) sprintf(_Logger->Message, "Time to execute ReadPcbInputs,%u milliseconds", millis() - readPcbInputsTime);
	_Logger->Information(3, Data.Id);
	_LastReadTime = millis();
}

void Pcb_Epura::ProcessAndWriteData(int rawData){ // For Child
	if (_Logger->IsEnabledForLevel(5)) sprintf(_Logger->Message, "Live Pressure - rawData=%d", rawData);
	_Logger->Information(5, Data.Id);

	rawData = rawData - _CorrectionInputPressure;
	if (rawData < 0)  rawData = 0;

	if (_Logger->IsEnabledForLevel(5)) sprintf(_Logger->Message, "Data.LiveFilterPressure-rawData(avec correction), %d", rawData);
	_Logger->Information(5, Data.Id);

	float mappedRawData = float(map(rawData, 0, _AnalogPrecisionInput, 0, _SensorMaxH20 * 1000)) / 1000;

	Data.LiveFilterPressure = mappedRawData;

	if (_Logger->IsEnabledForLevel(5)) sprintf(_Logger->Message, "Data.LiveFilterPressure,%f", Data.LiveFilterPressure);
	_Logger->Information(5, Data.Id);

	//Mettre les 20 dernieres pression en stock avec indice 0 � la plus r�cente.
	for (int i = 19; i > 0; i--) {
		_PressureHistory[i] = _PressureHistory[i - 1];
	}
	_PressureHistory[0] = Data.LiveFilterPressure;
}

void Pcb_Epura::ReadPressureOnPin() { // For Child
	_Logger->Information(9, "ReadPressureOnPin()");
	uint16_t rawData;
	rawData = ReadAveragePinValue(PressurePin, 2);

	ModbusRTUServer.poll();
	
	//Convertir la Valeur de pression Si nous avons un capteur 0-10V  Ou 4-20ma
	sprintf(_Logger->Message, "Live Pressure - rawData=%u", rawData);
	_Logger->Information(5);
//	_DepartSensor = 0;
	if (_DepartSensor != 0){
	    rawData = map(rawData, _DepartSensor, AnalogPrecisionInput - BuiltInHighInputCountDropI, 0, AnalogPrecisionInput);
	}
	else{
		rawData = map(rawData, _DepartSensor, AnalogPrecisionInput - BuiltInHighInputCountDropV, 0, AnalogPrecisionInput);
	}

	sprintf(_Logger->Message, "LivePressure-rawData, %d", rawData);
	_Logger->Information(5);

	if (rawData < 0 ||
	    rawData > (AnalogPrecisionInput * 2))     rawData = 0;
	
	if (rawData > AnalogPrecisionInput)           rawData = AnalogPrecisionInput;


	sprintf(_Logger->Message, "LivePressure-rawData(sent To ParentController), %d", rawData);
	_Logger->Information(5);

	if (!Data.ThereIsScreenOnThisController)
		_Register->Write(&Data, Pressure_Address, rawData);
	else
		ProcessAndWriteData(rawData);
}

int  Pcb_Epura::ReadAveragePinValue(int pinValue, int nbIteration) { // For Child
	int totalInput = 0;

	for (int i = 1; i <= nbIteration; i++) {
		totalInput = totalInput + analogRead(pinValue);
		delayMicroseconds(500); //Delais de 0,5ms entre chaque lecture
	}
	int averagePinValue =int(totalInput / nbIteration);
	return (averagePinValue);
}

void Pcb_Epura::InitPcbJob(){ // For Child and Parent
	if(Data.ThereIsScreenOnThisController)
		_Nextion->Write("Loading.progressBar.val",70);

	_Register->InitModbus(&Data);

	switch(Data.ControllerType){
		case Data.PARENT_CONTROLLER_TYPE:
			_Nextion->Init(_Rtc, _CodeVersion);
			break;

		case Data.CHILD_CONTROLLER_TYPE:
			InitChildControllerPins();
			break;
	}

}

void Pcb_Epura::InitChildControllerPins(){ // For Child
	pinMode(0, OUTPUT); // D-OUT3
	pinMode(1, OUTPUT); // D-OUT2
	pinMode(8, OUTPUT); // D-OUT1
	digitalWrite(0, LOW);
	digitalWrite(1, LOW);
	digitalWrite(8, LOW);
}

void Pcb_Epura::DelayWithPoll(uint32_t timeGap) { // For Child
	uint32_t startTime = millis();
	while (millis() - startTime < timeGap) 
		ModbusRTUServer.poll();
}

void Pcb_Epura::WriteInitializedValueInRegister(){ // For Child
	_Logger->Information(6, Data.Id, "D�marrage de la carte audio,111"); // ??
	if (_Register->Read(&Data, ChildControllerStateAddress) == Data.STATE_READYINLOOP) { // Indique que le master voit le slave

		// �criture du RTC 
		_Register->Write(&Data, YearRegister, _Rtc->getYear());
		_Register->Write(&Data, MonthRegister, _Rtc->getMonth());
		_Register->Write(&Data, DayRegister, _Rtc->getDay());
		_Register->Write(&Data, HourRegister, _Rtc->getHours());
		_Register->Write(&Data, MinuteRegister, _Rtc->getMinutes());
		_Register->Write(&Data, SecondRegister, _Rtc->getSeconds());
		_Register->Write(&Data, AuthorizedForSound_Address, 0);

		if (_Logger->IsEnabledForLevel(1))sprintf(_Logger->Message, "Date from master to slave(%d): %d/%d/%d %d:%d:%d",
		Data.Id, _Rtc->getYear(), _Rtc->getMonth(), _Rtc->getDay(), _Rtc->getHours(), _Rtc->getMinutes(), _Rtc->getSeconds());
		_Logger->Information(1, 0);

		_Register->Write(&Data, SoundNumber_Address, _Audio->_SoundId);
		}


}

void Pcb_Epura::ReadInitializedValueFromRegister(){ // For Child
	_Logger->Information(9, "ReadInitializedValueFromRegister()");

	_Rtc->setYear(_Register->Read(&Data, YearRegister));
	_Rtc->setMonth(_Register->Read(&Data,MonthRegister));
	_Rtc->setDay(_Register->Read(&Data, DayRegister));
	_Rtc->setHours(_Register->Read(&Data, HourRegister));
	_Rtc->setMinutes(_Register->Read(&Data, MinuteRegister));
	_Rtc->setSeconds(_Register->Read(&Data, SecondRegister));
	_RtcUpdated = true;
	if (_Logger->IsEnabledForLevel(6)) sprintf(_Logger->Message, "Date synchronized with master: 20%d/%d/%d %d:%d:%d",
															_Rtc->getYear(), _Rtc->getMonth(), _Rtc->getDay(),
															_Rtc->getHours(), _Rtc->getMinutes(), _Rtc->getSeconds());
	_Logger->Information(6);

	WriteVersionInRegister();

	_Register->Write(&Data, SyncState_Address, SYNCHED_PARENT_TO_CHILD);
}

void Pcb_Epura::WriteVersionInRegister(){ // For Child
	long versions[3] = {0,0,0};
	int noVersion = 0;
	
	for(int pos = 0; pos < 10; pos++){
		if (int(_CodeVersion[pos]) > 47 && int(_CodeVersion[pos]) < 58) // verif si c'est un caract�re num�rique
		versions[noVersion] = versions[noVersion] * 10 + (int(_CodeVersion[pos]) - 48);
		else{
			if (_CodeVersion[pos] == '.')
			noVersion++;
			else
			break;
		}
	}

	sprintf(_Logger->Message, "%d - %d - %d", versions[0], versions[1], versions[2]);
	_Logger->Information(6);

	_Register->Write(&Data, CodeVersionMajor_Address,    versions[0]);
	_Register->Write(&Data, CodeVersionMinor_Address,    versions[1]);
	_Register->Write(&Data, CodeVersionRevision_Address, versions[2]);
}

void Pcb_Epura::ReadVersionFromRegister(){ // For Parent
	uint16_t codeVersion[3];
	codeVersion[0] = _Register->Read(&Data, CodeVersionMajor_Address);
	codeVersion[1] = _Register->Read(&Data, CodeVersionMinor_Address);
	codeVersion[2] = _Register->Read(&Data, CodeVersionRevision_Address);

	char strVersion[10];
	sprintf(strVersion, "%u.%u.%u", codeVersion[0], codeVersion[1], codeVersion[2]);
	if (_Logger->IsEnabledForLevel(6)) sprintf(_Logger->Message, "Version du Programme du 'ChildController', %s", strVersion);
	_Logger->Information(6, Data.Id);
	_Register->Write(&Data, SyncState_Address, SYNCHED_COMPLETELY);

	char paramName[20];
	sprintf(paramName, "param.VersionChild%d.txt", Data.Pos + 1);
	_Nextion->Write(paramName, strVersion);
}

void Pcb_Epura::LogPressuresData() { // For Parent
	if (_Logger->IsEnabledForLevel(9)) 
		sprintf(_Logger->Message, "function, LogPressuresData(slavePos=%d)", Data.Pos);
		
	_Logger->Information(9, Data.Id);

	if (Data.Type == ENGIN_TYPE) {
		if (_Logger->IsEnabledForLevel(5)) sprintf(_Logger->Message, "LiveMotorPressure,%f", Data.LiveFilterPressure);
		_Logger->Information(5, Data.Id);
		if (_Logger->IsEnabledForLevel(6)) sprintf(_Logger->Message, "Pression filtre moteur,%f", Data.LiveFilterPressure);
		_Logger->Information(6, Data.Id);
	}

	if (Data.Type == CABIN_TYPE) {
		if (_Logger->IsEnabledForLevel(5)) sprintf(_Logger->Message, "LiveCabinFilterPressure,%f", Data.LiveFilterPressure);
		_Logger->Information(5, Data.Id);
		if (_Logger->IsEnabledForLevel(6)) sprintf(_Logger->Message, "Pression filtre cabine,%f", Data.LiveFilterPressure);
		_Logger->Information(6, Data.Id);
	}

}

void Pcb_Epura::LogStateChange() { // For Child and Parent
	switch (Data.ActualState) {
		case Data.STATE_NOT_READY:
			_Logger->Information(6, Data.Id, "Etat: Pas pret,I0");
			break;

		case Data.STATE_STOP_FAN:
			_Logger->Information(6, Data.Id, "Etat: Arret du ventilateur avant nettoyage,I2");
			break;

		case Data.STATE_PLAYING_SOUND:
			_Logger->Information(6, Data.Id, "Etat: Nettoyage en cours,I4");
			break;

		case Data.STATE_START_FAN:
			_Logger->Information(6, Data.Id, "Etat: Demarrage du ventilateur apres nettoyage,I6");
			break;
	}
}

void Pcb_Epura::ExecuteRoutineFromParent(){ // For Parent
	switch(_Register->Read(&Data, SyncState_Address)){
		case SYNC_ASKED:					WriteInitializedValueInRegister();	break;
		case SYNCHED_PARENT_TO_CHILD:	ReadVersionFromRegister();			break;
		case SYNCHED_COMPLETELY:		    ReadPressureInRegister();			break;

	}
	
    CheckForStateChange(EXECUTED_ON_PARENT);
}

void Pcb_Epura::SetShowOnScreen(int isPressureIsShownOnScreen)				{Data.ShowDataOnScreen = isPressureIsShownOnScreen;}
void Pcb_Epura::Poll(void)													{ModbusRTUServer.poll();}
void Pcb_Epura::SetCleanUpAtStartup(int value)								{_CleanUpAtStartup = (value) ? true : false;}
void Pcb_Epura::ReadSoundIdFromRegister()									{_Audio->SetFileName(_Register->Read(&Data, SoundNumber_Address));}
void Pcb_Epura::SetTimeBetweenRead(int timeBetweenRead)						{_TimeBetweenRead = timeBetweenRead;}
void Pcb_Epura::SetTempsMsOverMaxPressure(int tempsMsOverMaxPressure)		{_TempsMsOverMaxPressure = tempsMsOverMaxPressure;}
void Pcb_Epura::SetMaxPressureTrailingStop(int maxPressureTrailingStop)		{_MaxPressureTrailingStop = maxPressureTrailingStop;}
void Pcb_Epura::SetTimeGapAfterFanStart(int timeGapAfterFanStart)			{_TimeGapAfterFanStart = timeGapAfterFanStart;}
void Pcb_Epura::SetTimeGapAfterFanStop(int timeGapAfterFanStop)				{_TimeGapAfterFanStop = timeGapAfterFanStop;}
void Pcb_Epura::SetTimeGapAfterSound(int timeGapAfterSound)					{_TimeGapAfterSound = timeGapAfterSound;}
void Pcb_Epura::SetCorrectionInputPressure(int correctionInputPressure)		{_CorrectionInputPressure = correctionInputPressure;}
void Pcb_Epura::SetMaxFilterPressureAbsolute(int maxFilterPressureAbsolute)	{Data.AbsoluteMaxPressure = maxFilterPressureAbsolute;}
void Pcb_Epura::SetMaxFilterPressure(int maxFilterPressure)					{Data.MaxFilterPressure = maxFilterPressure;}
void Pcb_Epura::SetSound(int soundId)										{_Audio = new Audio_Epura(_Logger, _Register, soundId);}
void Pcb_Epura::SetSensorMaxH20(int sensorMaxH20)							{_SensorMaxH20 = sensorMaxH20;}
void Pcb_Epura::SetChildMode(int childMode)									{_ChildMode = childMode;}
