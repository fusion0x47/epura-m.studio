#include "Register_Epura.h"

const int LED_PIN =  LED_BUILTIN;

Register_Epura::Register_Epura(Logger_Epura* logger){
	_Logger = logger;
}

void Register_Epura::InitModbus(PcbData* filter){
	switch(filter->ControllerType){
		case filter->PARENT_CONTROLLER_TYPE:
			if (!ModbusRTUClient.begin(9600)) 		_Logger->Error(6, 0, "Etat: Echec du demarrage de la communication Modbus,I106", "ECHEC COMMUNICATION MODBUS");
			else 									_Logger->Information(6, 0, "Etat: Demarrage de la communication Modbus,I107");
			delay(2000);
			break;
			
		case filter->CHILD_CONTROLLER_TYPE:
			if (!ModbusRTUServer.begin(filter->Id, 9600)) { //MODBUS COMMUNICATION ON SERIAL2
				_Logger->Error(7, -10, "Failed to start Modbus RTU Server!");
				exit(0);
			}
			else{
				_Logger->Information(7, "SUCCESS start Modbus RTU Server!");
				ModbusRTUServer.configureHoldingRegisters(0, 30); //Init Registers
				InitStateHistoryRegisterValues(filter);
			}
			break;
	}
}

void Register_Epura::InitStateHistoryRegisterValues(PcbData* filter) {
	for (int registerAddress = 0; registerAddress < 30; registerAddress++) {
		Write(filter, registerAddress, 0); // Init all value to 0
		for (int noState = 0; noState < 10; noState++) {
			_LastRegisterValue[noState][registerAddress] = 0;
		}
	}
}

void Register_Epura::WaitUntilCommsAreReady(){
	bool isDelayed = false;
	while(!(millis() - _TimerBetweenComm > _DelayBetweenComm)){
		if (!isDelayed){
			sprintf(_Logger->Message, "Delay needed(%u)", millis() - _TimerBetweenComm);
			_Logger->Information(2, 0);
			isDelayed = true;
		}
		delay(100);
	}
}

bool Register_Epura::Write(PcbData* filter, int registerAddress, uint16_t value, uint8_t nbRetry) {
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, Register.Write(%d & %d & %u)", filter->Pos, registerAddress, value);
	_Logger->Information(9, filter->Id);

	WaitUntilCommsAreReady();
	int result;
	switch(filter->ControllerType){
		case filter->PARENT_CONTROLLER_TYPE: result = ModbusRTUClient.holdingRegisterWrite(filter->Id, registerAddress, value); break;
		case filter->CHILD_CONTROLLER_TYPE : result = ModbusRTUServer.holdingRegisterWrite(registerAddress, value);             break;
	}
	digitalWrite(LED_PIN, HIGH);
	delay(10);
	digitalWrite(LED_PIN, LOW);
	_TimerBetweenComm = millis();

	int addressPosInArray = registerAddress - 10;
	if (_LastRegisterValue[filter->Pos][addressPosInArray] != value) {
		if (registerAddress == ChildControllerStateAddress)		filter->ActualState = value;

		if (_Logger->IsEnabledForLevel(2)) sprintf(_Logger->Message, "holdingRegisterWrite(%d & %d & %u), oldValue=%u, newValue=%u",
																	filter->Id, registerAddress, value,
																	_LastRegisterValue[filter->Pos][registerAddress - 10], value);
		_Logger->Information(2, filter->Id);

		if (registerAddress == ChildControllerStateAddress || registerAddress == SyncState_Address || registerAddress == AuthorizedForSound_Address)
			_Logger->Information(1, filter->Id);
	}
	
	_LastRegisterValue[filter->Pos][addressPosInArray] = value;
	
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, WriteRegister return=%d", result);
	_Logger->Information(9, filter->Id);
	
	return (result);
}

void Register_Epura::DelayOfMilliseconds(uint32_t timeGap) {
	uint32_t startTime = millis();
	while (millis() - startTime < timeGap) {}
}

uint16_t Register_Epura::Read(PcbData* filter, int registerAddress, uint8_t nbRetry) {
	unsigned long ReadRegisterTime = millis();
	WaitUntilCommsAreReady();
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, ReadRegister(%d & %d & %d)", filter->Pos, registerAddress, filter->Id);
	_Logger->Information(9, filter->Id);
	digitalWrite(LED_PIN, HIGH) ;
	delay(10);
	digitalWrite(LED_PIN, LOW);

	uint16_t result = 65535;
	switch (filter->ControllerType){
		case filter->PARENT_CONTROLLER_TYPE:	result = ModbusRTUClient.holdingRegisterRead(filter->Id, registerAddress); break;
		case filter->CHILD_CONTROLLER_TYPE :	result = ModbusRTUServer.holdingRegisterRead(registerAddress); break;
	}
	_TimerBetweenComm = millis();
	delay(5);
	
	if (result == 65535 && nbRetry < 2) {
		digitalWrite(LED_PIN, HIGH);

		if (_Logger->IsEnabledForLevel(2)) sprintf(_Logger->Message, "retrying(%d) - LastError: %s", nbRetry, ModbusRTUClient.lastError());
		_Logger->Information(2, filter->Id);

		if (_Logger->IsEnabledForLevel(6)) sprintf(_Logger->MessageForScreen, "Etat: Communication perturbe (%d),W98", nbRetry);
		if (_Logger->IsEnabledForLevel(6)) sprintf(_Logger->Message, "ERR-Read ChildId=%d Retry=%d reg=%d", filter->Id, nbRetry, registerAddress);
		_Logger->Warning(6, filter->Id, -10, _Logger->Message, _Logger->MessageForScreen);
		digitalWrite(LED_PIN, LOW);
		
		Read(filter, registerAddress, nbRetry + 1);
		}
	else{
		if (result != _LastRegisterValue[filter->Pos][registerAddress - 10]) {
			if (_Logger->IsEnabledForLevel(2) || _Logger->IsEnabledForLevel(1))
			sprintf(_Logger->Message, "holdingRegisterRead(slaveId=%d & %d), oldValue=%u, newValue=%u, nbRetry=%u",
			filter->Id, registerAddress,
			_LastRegisterValue[filter->Pos][registerAddress - 10], result, nbRetry);
			_Logger->Information(2, filter->Id);
			if (_Logger->IsEnabledForLevel(1) && (registerAddress == ChildControllerStateAddress ||
			registerAddress == SyncState_Address ||
			registerAddress == AuthorizedForSound_Address)) {
				_Logger->Information(1, filter->Id);
			}

			_LastRegisterValue[filter->Pos][registerAddress - 10] = result;


		}
	}

	sprintf(_Logger->Message, "ReadRegister return=%u in %u milliseconds", result, millis() - ReadRegisterTime);
	_Logger->Information(3, filter->Id);
	return (result);
}

void Register_Epura::SetDelayBetweenComm(int delayBetweenComm){_DelayBetweenComm = delayBetweenComm;}
