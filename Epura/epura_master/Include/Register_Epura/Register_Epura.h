#ifndef REGISTER_H_
	#define REGISTER_H_

	#include "../Logger_Epura/Logger_Epura.h"
	#include "../Pcb_Epura/PcbData.h"
	#include "../ArduinoModbus/ArduinoModbus.h"

	static const int Pressure_Address                   = 10;
	static const int AuthorizedForSound_Address         = 11;
	static const int ChildControllerStateAddress        = 12;
			
	static const int SoundNumber_Address                = 13;
	static const int YearRegister                       = 14;
	static const int MonthRegister                      = 15;
	static const int DayRegister                        = 16;
	static const int HourRegister                       = 17;
	static const int MinuteRegister                     = 18;
	static const int SecondRegister                     = 19;

	static const int SyncState_Address					= 26;
	static const int CodeVersionMajor_Address           = 27;
	static const int CodeVersionMinor_Address           = 28;
	static const int CodeVersionRevision_Address        = 29;
			
	static const uint8_t SYNC_NOT_INITIALIZED			= 0;
	static const uint8_t SYNC_ASKED						= 1;
	static const uint8_t SYNCHED_PARENT_TO_CHILD		= 3;
	static const uint8_t SYNCHED_COMPLETELY				= 4;

	class Register_Epura{
		private:
			// variables
			uint16_t        _LastRegisterValue[10][30]; //Le second paramÍtre est le rigister address - 10
			unsigned long   _TimerBetweenComm;
			int				_DelayBetweenComm = 0;
			
			// objets
			Logger_Epura*		_Logger;
		
			// fonctions
			void DelayOfMilliseconds(uint32_t timeGap);
			void InitStateHistoryRegisterValues(PcbData* filter);

		public:
			// constructeurs
			Register_Epura(Logger_Epura* logger);
				
			// fonctions
			void		InitModbus(PcbData* filter);
			bool		Write(PcbData* filter, int address, uint16_t value, uint8_t nbRetry = 0);
			uint16_t	Read(PcbData* filter, int address, uint8_t nbRetry = 0);
			bool		Write(int address, uint16_t value);
			void		WaitUntilCommsAreReady(void);
			void		SetDelayBetweenComm(int delayBetweenComm);

	};
#endif