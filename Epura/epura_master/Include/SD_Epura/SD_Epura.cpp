#include "SD_Epura.h"

const int ControllerId = 0;

SD_Epura::SD_Epura(Logger_Epura* logger, PCBList_Propulsa* pcbList, Register_Epura* registerFusion, Voltage_Epura* voltage, Nextion_Epura* nextion) {
	_Logger		= logger;
	_PcbList	= pcbList;
	_Register	= registerFusion;
	_Voltage	= voltage;
	_Nextion	= nextion;
	
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, SD_Begin()");
	_Logger->Information(9, ControllerId);
	_Logger->Information(6, ControllerId, "Etat: Initialisation de la carte Sd,I104");

	bool resultOfRead = SD.begin(_ChipSelect_SD);
	if (resultOfRead) {
		_Logger->Information(4, ControllerId, "Initializing SD card,card initialized.");
	}
	else {
		sprintf(_Logger->Message, "Initializing SD card,Card failed or not present");
		_Logger->Error(6, 601, _Logger->Message, "ERR - No SD Card");
	}

	sprintf(_Logger->Message, "function, SD_Begin() - return(%d)", resultOfRead);
	_Logger->Information(9, ControllerId);
}

void SD_Epura::ReadConfigFile() {
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, ReadConfigFromSDCard()");
	_Logger->Information(9, ControllerId);
	_Logger->Information(6, ControllerId, "Etat: Lecture des parametres,I105");

	File initvarfile = SD.open("config.txt", FILE_READ);

	if (initvarfile) {
		while (initvarfile.available()) {
			String RawConfigLine = initvarfile.readStringUntil('\n'); // ReadLine
			char configLine[RawConfigLine.length()];
			int pos;
			for (pos = 0; RawConfigLine.c_str()[pos] != '\r' && RawConfigLine.c_str()[pos] != '\0'; pos++) {
				configLine[pos] = RawConfigLine.c_str()[pos];
			}
			configLine[pos] = '\0';
			if(pos > 5){ // Pour ne pas traiter les lignes vides
				ProcessLineInConfigFile(configLine);
			}
		}
		initvarfile.close();
	}
}

void SD_Epura::ProcessLineInConfigFile(char *configLine, int lastPcbId) {
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, Verif_Sd_Variable(bufferChar=%s)", configLine);
	_Logger->Information(9, ControllerId);
	char noParamInChr[6] = {configLine[6], configLine[7], configLine[8], configLine[9], configLine[10], '\0'};
	
	Param_Epura param(configLine);
	if (param.PcbId == 0){ // Indique des param�tres g�n�raux � utiliser pour le 'master' ou le 'stand alone'
		switch (param.VariableId) {
			case 0: if (param.Value != 1) {
						sprintf(_Logger->Message, "ERR101, The value of the parameter PARAM_10000 is not 1");
						_Logger->Error(6, ControllerId, 101, _Logger->Message, "Invalid PARAM_10000");
					}
					else{
						_PcbList->ThisPcb->Data.ControllerType = int(param.Id / 10000); // PARENT OU CHILD
						switch(_PcbList->ThisPcb->Data.ControllerType){
							case _PcbList->ThisPcb->Data.PARENT_CONTROLLER_TYPE:	_PcbList->ThisPcb->Data.ThereIsScreenOnThisController = true; 
																			_Logger->SetDefaultValue(true, 0);
																			_Nextion->AddPcb(&(_PcbList->ThisPcb->Data));
																			break;
																			
							case _PcbList->ThisPcb->Data.CHILD_CONTROLLER_TYPE:	_PcbList->ThisPcb->Data.ThereIsScreenOnThisController = false; break;
						}
					}
			break;
			case 210: _PcbList->SetDelayBetweenLog(param.Value);						break;
			case 220: _Nextion->SetDelayBetweenDisplay(param.Value);					break;
			case 230: _Register->SetDelayBetweenComm(param.Value);						break;
			case 240: _Nextion->SetDelayForErrorMessage(param.Value);					break;
			case 450: _Voltage->SetVoltageLimit(param.Value, WARNING_TOP);	break;
			case 460: _Voltage->SetVoltageLimit(param.Value, WARNING_BOTTOM); break;
			case 470: _Voltage->SetVoltageLimit(param.Value, ERROR_TOP);		break;
			case 480: _Voltage->SetVoltageLimit(param.Value, ERROR_BOTTOM);	break;
			case 490: _Voltage->SetVoltageLimit(param.Value, DIFF_VOLTAGE);	break;
			case 500: _PcbList->ThisPcb->SetShowOnScreen(param.Value);				break;
			case 510: _PcbList->ThisPcb->SetSensorMaxH20(param.Value);				break;
			case 520: _PcbList->ThisPcb->SetCorrectionInputPressure(param.Value);	break;
			case 530: _PcbList->ThisPcb->SetMaxFilterPressure(param.Value);			break;
			case 540: _PcbList->ThisPcb->SetMaxFilterPressureAbsolute(param.Value);	break;
		}
	}
	else{
		Pcb_Epura* filter;
		switch(_PcbList->ThisPcb->Data.ControllerType){
			case _PcbList->ThisPcb->Data.PARENT_CONTROLLER_TYPE: filter = _PcbList->FindPcbById(param.PcbId); break;
			case _PcbList->ThisPcb->Data.CHILD_CONTROLLER_TYPE : filter = _PcbList->ThisPcb; break;
		}
		
		switch (param.VariableId) {
			case 0:
				switch(_PcbList->ThisPcb->Data.ControllerType){
					case _PcbList->ThisPcb->Data.PARENT_CONTROLLER_TYPE:	_PcbList->AddPcbData(&param); break;
				
					case _PcbList->ThisPcb->Data.CHILD_CONTROLLER_TYPE :	_PcbList->ThisPcb->Data.Id = param.Value; 
																	_PcbList->ThisPcb->Data.Pos = 0;
     																_Logger->SetDefaultValue(false, param.Value);
																	break;
				}
				break;
			case   5: filter->Data.Type = param.Value;						break;
			case  10: filter->SetChildMode(param.Value);					break;
			case 300: filter->SetTimeBetweenRead(param.Value);				break;
			case 320: filter->SetTempsMsOverMaxPressure(param.Value);		break;
			case 330: filter->SetMaxPressureTrailingStop(param.Value);		break;
			case 380: filter->SetSound(param.Value);						break;
			case 400: filter->SetCleanUpAtStartup(param.Value);				break;
			case 410: filter->SetTimeGapAfterFanStart(param.Value);			break;
			case 420: filter->SetTimeGapAfterFanStop(param.Value);			break;
			case 430: filter->SetTimeGapAfterSound(param.Value);			break;
			case 500: filter->SetShowOnScreen(param.Value);					break;
			case 510: filter->SetSensorMaxH20(param.Value);					break;
			case 520: filter->SetCorrectionInputPressure(param.Value);		break;
			case 530: filter->SetMaxFilterPressure(param.Value);			break;
			case 540: filter->SetMaxFilterPressureAbsolute(param.Value);	break;
		}
	}
}

int SD_Epura::FindValueInLine(char *configLine){
	char tempValue[40];
	for(int pos = 12; configLine[pos] != '\0'; pos++){
		tempValue[pos - 12] = configLine[pos];
	}
	return atoi(tempValue);
}
