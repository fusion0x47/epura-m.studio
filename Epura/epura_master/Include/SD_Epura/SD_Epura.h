#ifndef SD_EPURA_H_
	#define SD_EPURA_H_

	#include "../SD/SD.h"
	#include "../Logger_Epura/Logger_Epura.h"
	#include "../PcbList_Epura/PcbList_Epura.h"
	#include "../Nextion_Epura/Nextion_Epura.h"
	#include "../param_Epura/Param_Epura.h"
	#include "../Register_Epura/Register_Epura.h"
	#include "../Voltage_Epura/Voltage_Epura.h"

	const int _ChipSelect_SD = SDCARD_SS_PIN;

	class SD_Epura{
		private:
			// objets
			Logger_Epura*		_Logger;
			PCBList_Propulsa*		_PcbList;
			Register_Epura*	_Register;
			Voltage_Epura*		_Voltage;
			Nextion_Epura*		_Nextion;
			
			// fonctions
			void ProcessLineInConfigFile(char *configLine, int lastPcbId = -1);
			int FindValueInLine(char *configLine);

		public:
			// constructeurs
			SD_Epura(Logger_Epura* logger, PCBList_Propulsa* pcbList, Register_Epura* registerFusion, Voltage_Epura* voltage, Nextion_Epura* nextion);
		
			// fonctions
			void ReadConfigFile(void);

	};
#endif /* SD_EPURA_H_ */