#include "SD_Fusion0x47.h"

const int ControllerId = 0;

SD_Fusion0x47::SD_Fusion0x47(Logger_Fusion0x47* logger, Filters_Fusion0x47* filters, Register_Fusion0x47* registerFusion, Voltage_Fusion0x47* voltage, Nextion_Fusion0x47* nextion) {
	_Logger		= logger;
	_Filters	= filters;
	_Register	= registerFusion;
	_Voltage	= voltage;
	_Nextion	= nextion;
	
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, SD_Begin()");
	_Logger->Information(9, ControllerId);
	_Logger->Information(6, ControllerId, "Etat: Initialisation de la carte Sd,I104");

	bool resultOfRead = SD.begin(_ChipSelect_SD);
	if (resultOfRead) {
		_Logger->Information(4, ControllerId, "Initializing SD card,card initialized.");
	}
	else {
		sprintf(_Logger->Message, "Initializing SD card,Card failed or not present");
		_Logger->Error(6, 601, _Logger->Message, "ERR - No SD Card");
	}

	sprintf(_Logger->Message, "function, SD_Begin() - return(%d)", resultOfRead);
	_Logger->Information(9, ControllerId);
}

void SD_Fusion0x47::ReadConfigFile() {
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, ReadConfigFromSDCard()");
	_Logger->Information(9, ControllerId);
	_Logger->Information(6, ControllerId, "Etat: Lecture des parametres,I105");

	File initvarfile = SD.open("config.txt", FILE_READ);

	if (initvarfile) {
		while (initvarfile.available()) {
			String RawConfigLine = initvarfile.readStringUntil('\n'); // ReadLine
			char configLine[RawConfigLine.length()];
			int pos;
			for (pos = 0; RawConfigLine.c_str()[pos] != '\r' && RawConfigLine.c_str()[pos] != '\0'; pos++) {
				configLine[pos] = RawConfigLine.c_str()[pos];
			}
			configLine[pos] = '\0';
			if(pos > 5){ // Pour ne pas traiter les lignes vides
				ProcessLineInConfigFile(configLine);
			}
		}
		initvarfile.close();
	}
}

void SD_Fusion0x47::ProcessLineInConfigFile(char *configLine, int lastFilterId) {
	if (_Logger->IsEnabledForLevel(9)) sprintf(_Logger->Message, "function, Verif_Sd_Variable(bufferChar=%s)", configLine);
	_Logger->Information(9, ControllerId);
	char noParamInChr[6] = {configLine[6], configLine[7], configLine[8], configLine[9], configLine[10], '\0'};
	
	Param_Fusion0x47 param(configLine);
	if (param.FilterId == 0){ // Indique des param�tres g�n�raux � utiliser pour le 'master' ou le 'stand alone'
		switch (param.VariableId) {
			case 0: if (param.Value != 1) {
						sprintf(_Logger->Message, "ERR101, The value of the parameter PARAM_10000 is not 1");
						_Logger->Error(6, ControllerId, 101, _Logger->Message, "Invalid PARAM_10000");
					}
					else{
						_Filters->ThisFilter->Data.ControllerType = int(param.Id / 10000); // PARENT OU CHILD
						switch(_Filters->ThisFilter->Data.ControllerType){
							case _Filters->ThisFilter->Data.PARENT_CONTROLLER_TYPE:	_Filters->ThisFilter->Data.ThereIsScreenOnThisController = true; 
																			_Logger->SetDefaultValue(true, 0);
																			_Nextion->AddFilter(&(_Filters->ThisFilter->Data));
																			break;
																			
							case _Filters->ThisFilter->Data.CHILD_CONTROLLER_TYPE:	_Filters->ThisFilter->Data.ThereIsScreenOnThisController = false; break;
						}
					}
			break;
			case 210: _Filters->SetDelayBetweenLog(param.Value);						break;
			case 220: _Nextion->SetDelayBetweenDisplay(param.Value);					break;
			case 230: _Register->SetDelayBetweenComm(param.Value);						break;
			case 240: _Nextion->SetDelayForErrorMessage(param.Value);					break;
			case 450: _Voltage->SetVoltageLimit(param.Value, _Voltage->WARNING_TOP);	break;
			case 460: _Voltage->SetVoltageLimit(param.Value, _Voltage->WARNING_BOTTOM); break;
			case 470: _Voltage->SetVoltageLimit(param.Value, _Voltage->ERROR_TOP);		break;
			case 480: _Voltage->SetVoltageLimit(param.Value, _Voltage->ERROR_BOTTOM);	break;
			case 490: _Voltage->SetVoltageLimit(param.Value, _Voltage->DIFF_VOLTAGE);	break;
			case 500: _Filters->ThisFilter->SetShowOnScreen(param.Value);				break;
			case 510: _Filters->ThisFilter->SetSensorMaxH20(param.Value);				break;
			case 520: _Filters->ThisFilter->SetCorrectionInputPressure(param.Value);	break;
			case 530: _Filters->ThisFilter->SetMaxFilterPressure(param.Value);			break;
			case 540: _Filters->ThisFilter->SetMaxFilterPressureAbsolute(param.Value);	break;
		}
	}
	else{
		Filter_Fusion0x47* filter;
		switch(_Filters->ThisFilter->Data.ControllerType){
			case _Filters->ThisFilter->Data.PARENT_CONTROLLER_TYPE: filter = _Filters->FindFilterById(param.FilterId); break;
			case _Filters->ThisFilter->Data.CHILD_CONTROLLER_TYPE : filter = _Filters->ThisFilter; break;
		}
		
		switch (param.VariableId) {
			case 0:
				switch(_Filters->ThisFilter->Data.ControllerType){
					case _Filters->ThisFilter->Data.PARENT_CONTROLLER_TYPE:	_Filters->AddFilter(&param); break;
				
					case _Filters->ThisFilter->Data.CHILD_CONTROLLER_TYPE :	_Filters->ThisFilter->Data.Id = param.Value; 
																	_Filters->ThisFilter->Data.Pos = 0;
     																_Logger->SetDefaultValue(false, param.Value);
																	break;
				}
				break;
			case   5: filter->Data.Type = param.Value;						break;
			case  10: filter->SetChildMode(param.Value);					break;
			case 300: filter->SetTimeBetweenRead(param.Value);				break;
			case 320: filter->SetTempsMsOverMaxPressure(param.Value);		break;
			case 330: filter->SetMaxPressureTrailingStop(param.Value);		break;
			case 380: filter->SetSound(param.Value);						break;
			case 400: filter->SetCleanUpAtStartup(param.Value);				break;
			case 410: filter->SetTimeGapAfterFanStart(param.Value);			break;
			case 420: filter->SetTimeGapAfterFanStop(param.Value);			break;
			case 430: filter->SetTimeGapAfterSound(param.Value);			break;
			case 500: filter->SetShowOnScreen(param.Value);					break;
			case 510: filter->SetSensorMaxH20(param.Value);					break;
			case 520: filter->SetCorrectionInputPressure(param.Value);		break;
			case 530: filter->SetMaxFilterPressure(param.Value);			break;
			case 540: filter->SetMaxFilterPressureAbsolute(param.Value);	break;
		}
	}
}

int SD_Fusion0x47::FindValueInLine(char *configLine){
	char tempValue[40];
	for(int pos = 12; configLine[pos] != '\0'; pos++){
		tempValue[pos - 12] = configLine[pos];
	}
	return atoi(tempValue);
}
