#ifndef SD_FUSION0X47_H_
	#define SD_FUSION0X47_H_

	#include "../SD/SD.h"
	#include "../Logger_Fusion0x47/Logger_Fusion0x47.h"
	#include "../Filters_Fusion0x47/Filters_Fusion0x47.h"
	#include "../Nextion_Fusion0x47/Nextion_Fusion0x47.h"
	#include "../param_Fusion0x47/Param_Fusion0x47.h"
	#include "../Register_Fusion0x47/Register_Fusion0x47.h"
	#include "../Voltage_Fusion0x47/Voltage_Fusion0x47.h"

	class SD_Fusion0x47{
		private:
			// objets
			Logger_Fusion0x47*		_Logger;
			Filters_Fusion0x47*		_Filters;
			Register_Fusion0x47*	_Register;
			Voltage_Fusion0x47*		_Voltage;
			Nextion_Fusion0x47*		_Nextion;
			
			// fonctions
			void ProcessLineInConfigFile(char *configLine, int lastFilterId = -1);
			int FindValueInLine(char *configLine);

		public:
			// constructeurs
			SD_Fusion0x47(Logger_Fusion0x47* logger, Filters_Fusion0x47* filters, Register_Fusion0x47* registerFusion, Voltage_Fusion0x47* voltage, Nextion_Fusion0x47* nextion);
		
			// fonctions
			void ReadConfigFile(void);
			
			// constantes
			const int _ChipSelect_SD = SDCARD_SS_PIN;

	};
#endif /* SD_FUSION0X47_H_ */