#include "Voltage_Epura.h"

Voltage_Epura::Voltage_Epura(Logger_Epura* logger){
	_Logger = logger;
	pinMode(PIN_VOLTAGE, INPUT);
}

float Voltage_Epura::ReadVoltage(){
	_Logger->Information(9, 0, "ReadVoltage()");
	int rawVal;
	float voltage;
	
	voltage = float(analogRead(PIN_VOLTAGE)) / MAX_INPUT_PINA2 * MAX_VOLT_PINA2;
	
	if (voltage < _ErrorBottomLevelVoltage || voltage > _ErrorTopLevelVoltage){
		sprintf(_Logger->Message, "Erreur sur le voltage, %.2f V", voltage);
		_Logger->Error(6, -10, _Logger->Message);
	}
	else{
		if (voltage < _WarningBottomLevelVoltage || voltage > _WarningTopLevelVoltage){
			sprintf(_Logger->Message, "Avertissement sur le voltage, %.2f V", voltage);
			_Logger->Error(6, -10, _Logger->Message);
		}
		else{
			float DiffVoltage = (voltage - _LastVoltage < 0) ? _LastVoltage - voltage : voltage - _LastVoltage;
			if (DiffVoltage >= (float(_DiffVoltageForLogging) * 0.01)){
				sprintf(_Logger->Message, "Voltage, %.2f V", voltage);
				_Logger->Information(6, 0);
			}
		}
	}

	sprintf(_Logger->Message, "Return value of ReadVoltage()= %.2f V", voltage);
	_Logger->Information(9, 0, "ReadVoltage()");
	return voltage;
}

void Voltage_Epura::SetVoltageLimit(float limitValue, uint8_t limitType){
	switch(limitType){
		case WARNING_TOP:   _WarningTopLevelVoltage		= limitValue; break;
		case ERROR_TOP:     _ErrorTopLevelVoltage		= limitValue; break;
		case WARNING_BOTTOM:_WarningBottomLevelVoltage	= limitValue; break;
		case ERROR_BOTTOM:  _ErrorBottomLevelVoltage	= limitValue; break;
		case DIFF_VOLTAGE:  _DiffVoltageForLogging		= limitValue; break;
	}
}