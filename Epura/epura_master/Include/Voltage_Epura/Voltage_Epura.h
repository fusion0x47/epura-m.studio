#ifndef VOLTAGE_EPURA_H_
	#define VOLTAGE_EPURA_H_

	#include "Arduino.h"
	#include "wiring_private.h"
	#include "../Logger_Epura/Logger_Epura.h"

	static const uint8_t WARNING_TOP		= 0;
	static const uint8_t ERROR_TOP			= 1;
	static const uint8_t WARNING_BOTTOM	    = 2;
	static const uint8_t ERROR_BOTTOM		= 3;
	static const uint8_t DIFF_VOLTAGE		= 4;
			
	static const int	 PIN_VOLTAGE		= A2;
	static const int	 MAX_INPUT_PINA2	= 8192;
	static const int	 MAX_VOLT_PINA2		= 33;

	class Voltage_Epura{
		private:
			// objets
			
			Logger_Epura* _Logger;
			// variables
			float _LastVoltage					= 0;
			float _WarningTopLevelVoltage		= -1;
			float _WarningBottomLevelVoltage	= -1;
			float _ErrorTopLevelVoltage			= -1;
			float _ErrorBottomLevelVoltage		= -1;
			float _DiffVoltageForLogging		= -1;

		public:
			// constructeurs
			Voltage_Epura(Logger_Epura* logger);
			
			// fonctions
			float ReadVoltage(void);
			void SetVoltageLimit(float limitValue, uint8_t limiType);
			
	};




#endif /* VOLTAGE_EPURA_H_ */