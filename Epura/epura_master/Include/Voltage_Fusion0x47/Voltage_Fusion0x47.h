#ifndef VOLTAGE_FUSION0X47_H_
	#define VOLTAGE_FUSION0X47_H_

	#include "Arduino.h"
	#include "wiring_private.h"
	#include "../Logger_Fusion0x47/Logger_Fusion0x47.h"

	class Voltage_Fusion0x47{
		private:
			// objets
			
			Logger_Fusion0x47* _Logger;
			// variables
			float _LastVoltage					= 0;
			float _WarningTopLevelVoltage		= -1;
			float _WarningBottomLevelVoltage	= -1;
			float _ErrorTopLevelVoltage			= -1;
			float _ErrorBottomLevelVoltage		= -1;
			float _DiffVoltageForLogging		= -1;

		public:
			// constructeurs
			Voltage_Fusion0x47(Logger_Fusion0x47* logger);
			
			// fonctions
			float ReadVoltage(void);
			void SetVoltageLimit(float limitValue, uint8_t limiType);
			
			// constantes
			static const uint8_t WARNING_TOP		= 0;
			static const uint8_t ERROR_TOP			= 1;
			static const uint8_t WARNING_BOTTOM	= 2;
			static const uint8_t ERROR_BOTTOM		= 3;
			static const uint8_t DIFF_VOLTAGE		= 4;
			
			static const int	  PIN_VOLTAGE		= A2;
			static const int	  MAX_INPUT_PINA2	= 8192;
			static const int	  MAX_VOLT_PINA2	= 33;
		
	};




#endif /* VOLTAGE_FUSION0X47_H_ */