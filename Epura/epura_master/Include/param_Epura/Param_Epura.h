#ifndef PARAM_EPURA_H_
	#define PARAM_EPURA_H_

	class Param_Epura{
		public:
			// variables
			int Id;
			int Value;
			int PcbId;
			int VariableId;
	
			// constructeurs
			Param_Epura(char *configLine);

			// fonctions
			int FindValueInLine(char *configLine);
	};
#endif /* PARAM_EPURA_H_ */