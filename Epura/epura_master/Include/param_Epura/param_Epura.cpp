#include "Param_Epura.h"
#include "../Logger_Epura/Logger_Epura.h"
//#include <stdlib.h>

Param_Epura::Param_Epura(char *newConfigLine){
	char noParamInChr[6] = {newConfigLine[6], newConfigLine[7], newConfigLine[8], newConfigLine[9], newConfigLine[10], '\0'};
	Id = atoi(noParamInChr);
	Value = FindValueInLine(newConfigLine);
	PcbId = ((Id % 10000) / 1000) ;
	VariableId = (Id % 1000);
}
	
int Param_Epura::FindValueInLine(char *configLine){
	char tempValue[40];
	for(int pos = 12; configLine[pos] != '\0' && configLine[pos] != '\r' ; pos++){
		tempValue[pos - 12] = configLine[pos];
	}
	return atoi(tempValue);
}

