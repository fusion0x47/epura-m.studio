#ifndef PARAM_FUSION0X47_H_
	#define PARAM_FUSION0X47_H_

	class Param_Fusion0x47{
		public:
			// variables
			int Id;
			int Value;
			int FilterId;
			int VariableId;
	
			// constructeurs
			Param_Fusion0x47(char *configLine);

			// fonctions
			int FindValueInLine(char *configLine);
	};
#endif /* PARAM_FUSION0X47_H_ */