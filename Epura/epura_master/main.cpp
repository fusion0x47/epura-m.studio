﻿#include "./Include/Logger_Fusion0x47/Logger_Fusion0x47.h" 
#include "./Include/Filters_Fusion0x47/Filters_Fusion0x47.h"
#include "./Include/Nextion_Fusion0x47/Nextion_Fusion0x47.h"
#include "./Include/Register_Fusion0x47/Register_Fusion0x47.h"
#include "./Include/Voltage_Fusion0x47/Voltage_Fusion0x47.h"
#include "./Include/SD_Fusion0x47/SD_Fusion0x47.h"
#include "./Include/RTCZero/RTCZero.h"
#include "./Include/ArduinoRS485/ArduinoRS485.h"
#include "./Include/Audio_Fusion0x47/Audio_Fusion0x47.h"
#include "./Include/SDU/SDU.h"

const char CODE_VERSION[10] = "2.0.82";

#define RS845_DEFAULT_DE_PIN 30
#define RS845_DEFAULT_RE_PIN -1

const int LED_PIN			= LED_BUILTIN;
const int CE_RTC_PIN		= 19;

// variables a vérifier leurs noms et leurs utilités avec Éric
const int AnalogResForCpu	= 13;
const int PressurePin		= A1;
const int PinAnalogMode		= 31;

SD_Fusion0x47*			Sd;
Logger_Fusion0x47*		Logger;
Nextion_Fusion0x47*		Nextion;
Filters_Fusion0x47*		Filters;
Register_Fusion0x47*	Register;
Voltage_Fusion0x47*		Voltage;
RTCZero*				RtcMain;

void setup() {
	// Initialisation des objects
	RtcMain		= new RTCZero();
	Logger		= new Logger_Fusion0x47(RtcMain);
	Logger->EnableLevelsForSerial(3, 6, 9);
//	Logger->EnableLevelsForSDCard(3, 6);
	Register	= new Register_Fusion0x47(Logger);
	Voltage		= new Voltage_Fusion0x47(Logger);
	Nextion		= new Nextion_Fusion0x47(Logger, Register);
	Filters		= new Filters_Fusion0x47(Nextion, Logger, Register);
	Filters->ThisFilter	= new Filter_Fusion0x47((char*)&CODE_VERSION, Nextion, Logger, Register);
	Sd			= new SD_Fusion0x47(Logger, Filters, Register, Voltage, Nextion);
	

	Serial.begin(9600);//USB
	Serial1.begin(9600);// RS485

	// pinMode(PIN_MANUAL_CLEANUP, INPUT); doit être transférer dans le ChildController
	pinMode(LED_PIN, OUTPUT);
	pinMode(CE_RTC_PIN, OUTPUT);
	pinMode(PinAnalogMode, INPUT);
	digitalWrite(CE_RTC_PIN, LOW);
    pinPeripheral(PIN_SERIAL2_RX, PIO_SERCOM_ALT);
    pinPeripheral(PIN_SERIAL2_TX, PIO_SERCOM_ALT);

	Logger->Information(6, 0, "Etat: Demarrage du systeme en cours...,I100");
	sprintf(Logger->Message, "Version du micro-logiciel: %s,I101", CODE_VERSION);
	Logger->Information(6, 0);

	RtcMain->begin();

	Sd->ReadConfigFile();
	
	Filters->ThisFilter->InitFilterJob();
  
	analogReadResolution(AnalogResForCpu);

	Logger->Information(6, 0, "Etat: Demarrage systeme complete,I109");
}

void loop() {
	unsigned long loopStartTime = millis();
	Logger->Information(9, 0, "function, loop()");

	switch(Filters->ThisFilter->Data.ControllerType){
		case Filters->ThisFilter->Data.PARENT_CONTROLLER_TYPE:
			Voltage->ReadVoltage();
			Nextion->UpdateRtcWithNextion(RtcMain);
			Filters->ManageSoundPlaying();
			Nextion->ReceiveInteractionFromScreen();
			Filters->ExecuteRoutineForAllFilters();
			Filters->ThisFilter->ReadPressureOnPin();
			Nextion->ManageUpdate();
			break;
		
		case Filters->ThisFilter->Data.CHILD_CONTROLLER_TYPE:	
			
			Filters->ThisFilter->RoutineForChildController(); 
			break;
	}

	if (millis() - loopStartTime > 5) {
		if (Logger->IsEnabledForLevel(3)) sprintf(Logger->Message, "Time to execute main program,%u milliseconds", millis() - loopStartTime);
		Logger->Information(3, 0);
	}
}